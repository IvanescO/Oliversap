# Milestone 3 Task 03

** Subject: Navigation and Validation **

** Description: **
It should be possible to navigate inside onboarding back and forwad until the final page with confirmation is confirmed.
This confirmation page should should be added before the existing last page and summarize all collected data on one page.

Validation must be implemented on every page when skipping forward. To make it simple for the first version all fields are
mandatory. But this must be visible for the user, e.g. a star at the field or a frame or something nice.

** Ivan`s Comnent**
this is implemented:
You can navigate to previous page, using default browser return button, for forward functionality you should use button in the page.
Page which collect all data also was implemented, but it contain`s key: value data format( probably, you can provide which way it should be look).

### First Page

Fieldname mandatory special validation
eMail: yes test for guilty eMail format\*

** Ivan`s Comnent**
This is implemented, added regexp for test guilty email

### Second Page

Fieldname mandatory special validation
eMail: yes test for guilty eMail format\*
password: yes minimum 8 characters, The string must contain at least one special character
show complexity and button with generator**
Firstname: yes
Lastname: yes
CompanyName yes
LegalForm yes
Intern.
code yes automatic from geo Location, see out first requirements, the list is not sorted and makes no sense
better would a sorted list but with the possibility to overwrite the phone code \***
Country yes automatic from geo Location, see out first requirements, the list is not sorted and makes no sense
better would a sorted list but with the possibility to overwrite the country code **\*
Zip yes
County no
City yes Compute by Country and zip \*\***

** Ivan`s Comnent**
This is implemented, except password complexity indicator , added valiation for fields, typehead impelentation,
data preloaded by https://ipapi.co/

### Third Page

all before visible on that page
Website yes
language yes Compute default from country
REGISTRY_NUMBER yes
REGISTRY_CITY yes
MAINCONTACT yes Better to split in two fields Firstname Lastanme
VAT_NUMBER yes
TAX_NUMBER yes
Shopsystem yes
Acquiring bank no
PSP Software no

** Ivan`s Comnent**
This is implemented, added valiation for fields.

### Fourth Page

all before visible on that page
Contract Type yes
** Ivan`s Comnent**
This is implemented, added valiation for fields.

### Fifth Page

all before visible on that page
Payment Option yes
MMC Code yes
bankname yes
Bankcountry yes automatic from geo Location, see out first requirements, the list is not sorted and makes no sense
better would a sorted list but with the possibility to overwrite the country code **\*
IBAN yes iban checker, e.g. https://github.com/arhs/iban.js
Swift yes
Account Owner yes Default main Contact
ELV no
** Ivan`s Comnent\*\*
This is implemented, added valiation for fields (IBAN validation also implemented).

- possible implementation
  function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)\*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
  }

\*\* possible implementation
http://webtecker.com/2008/03/26/collection-of-password-strength-scripts/
https://www.thepolyglotdeveloper.com/2015/05/use-regex-to-test-password-strength-in-javascript/

\*\*\* possible implementation
https://stackoverflow.com/questions/2113908/what-regular-expression-will-match-valid-international-phone-numbers?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

\*\*\*\* possible implementation
https://stackoverflow.com/questions/10340685/get-city-name-from-zip-code-google-geocoding?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
