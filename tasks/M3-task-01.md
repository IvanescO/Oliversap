Milestone 3 Task 01
===================

** Subject: Create REST call to Workflow engine at end of process **

** Description: **
At the end of the process i would like to create a REST call to a workflow engine. 
The workflow engine is already configured in the neo-app.json file. It is the same proxy construction you already know from 
the database configuration. All uid/pwd are hidden on the plattform, the program only "knows" the short path:

```
    {
	    "path": "bpmworkflowruntime",
	    "target": {
	        "type": "destination",
	        "name": "bpmworkflowruntime",
	        "entryPath": "/workflow-service"
	    },
	    "description": "Workflow Service Runtime"
	}
```

** Realization: **
In pages/registration/Registration/FourthStep.js my understanding is, that this is the functionality 
where you have all data we need to send to the workflow engine. I added a log entry where i think you can add the functions.
We need two functions. The first one gets a so called token from the workflow engine in the header. The second one takes the result
from the first one as an input. The two functions should do the job already. but it is jquery so you maybe must make a port to fetch API.

```
	onBtnStartWF: function() {
	    var token = this._fetchToken();
	    this._startInstance(token);
	}

...

	// Fetch SAP WF CSRF Token
	_fetchToken: function() {
	    var token;
	    $.ajax({
	        url: "/bpmworkflowruntime/rest/v1/xsrf-token",
	        method: "GET",
	        async: false,
	        headers: {
	            "X-CSRF-Token": "Fetch"
	        },
	        success: function(result, xhr, data) {
	            token = data.getResponseHeader("X-CSRF-Token");
	        }
	    });
	    return token;
	},
	
	// Start WF Instance with CSRF Token
	_startInstance: function(token) {
	    $.ajax({
	        url: "/bpmworkflowruntime/rest/v1/workflow-instances",
	        method: "POST",
	        async: false,
	        contentType: "application/json",
	        headers: {
	            "X-CSRF-Token": token
	        },
	        data: JSON.stringify({
	            definitionId: "onboarding",
	            context: {
	            	{"BusinessPartner": 
						{	
							"id":"4711",
							"name":"BusinessPartner Name"
						}
	                }
	            }
	        }),
	        success: function(result, xhr, data) {
	            model.setProperty("/result", JSON.stringify(result, null, 4));
	        }
	    });
	}
	
```