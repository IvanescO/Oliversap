# Milestone 4 Task 01

** Subject: Navigation **

** Description: **
Add Navigation Button Back (German: Zurück) in the same color like the button with the normal back function you already implemented in milestone 3 - see your own comment


** Ivan`s Comnent**
this is implemented:
You can navigate to previous page, using default browser return button, for forward functionality you should use button in the page.
Page which collect all data also was implemented, but it contain`s key: value data format( probably, you can provide which way it should be look).



** Subject: Validation Page **

** Description: **
The workflow should start AFTER the user has confirmed all informations on the last validation page. It must be possible to change all values by clicking back and change. On the validation page in the current version there are 
- missing all true/false values (PRIVACY, PRIVACYUSE, DEBIT_AUTHORIZATION). Please fix that. See attached file Abweichungen_Onboarding.docx page 3
- some double values??? like COUNTRY and COUNTRY_CODE? PHONE and PHONE_NUMBER, Please check and fix that See attached file Abweichungen_Onboarding.docx page 3
- please format the validation page to a two column layout. See attached file Abweichungen_Onboarding.docx page 4


** Subject: Layout and Fonts **

** Description: **
Please change fonts. Missing to do from last Milestone.
Google fonts:
RALEWAY – for Headline
ROBOTO  – for normal text
Please reformat/resize Pages 1-5. On an Ipad Pro 1366 x 1040 px (100%) the pages including the buttons at the end must be visible. I took this resolution because you implemented it that way that beginnung from that resolution the overview navigation is switching to the left side. This brings enough space to show all fields. Please fill free to delete an empty row to reach the target. Our design chief Ingrid Haug mail@ingridhaug.com will do the final tests, so please involve her if you have any questions.


