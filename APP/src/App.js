import React, { Component } from "react";
import "./App.css";
import "react-bootstrap-typeahead/css/Typeahead.css";
import Routing from "./routing";

class App extends Component {
  render() {
    return (
      <div>
        <Routing />
      </div>
    );
  }
}

export default App;
