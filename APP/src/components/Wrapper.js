import React from "react";
import Header from "./Header";

const Wrapper = ({ children, headerColor, hideButtons }) => (
  <div>
    <Header color={headerColor} hideButtons={hideButtons} />
    {children}
  </div>
);
export default Wrapper;
