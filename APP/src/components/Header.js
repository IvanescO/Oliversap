import React from "react";
import logo from "../images/ClearVAT_weiss_klein.png";
import { Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

const Header = props => {
  return (
    <header className={"header clearfix " + props.color}>
      <Row>
        <Col lg={6} md={6} sm={7} xs={12} className="header-col">
          <Link to="/">
            <span className="logo">
              <img src={logo} alt="logo" />
            </span>
            <span className="logo" style={{ padding: 10, fontSize: 10 }}>
              v3.4
            </span>
          </Link>
        </Col>

        <Col lg={6} md={6} sm={5} xs={12} className="header-col">
          {!props.hideButtons && (
            <div>
              <button className="menu-btn">Log in</button>
              <button className="menu-btn active">Neu heir</button>
            </div>
          )}
        </Col>
      </Row>
    </header>
  );
};
export default Header;
