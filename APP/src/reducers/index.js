import { combineReducers } from "redux";
import FormReducer from "./FormReducer";
import CountriesInfoReducer from "./CountriesInfoReducer";
import { reducer as reduxFormReducer } from "redux-form";

export default combineReducers({
  FormReducer,
  CountriesInfoReducer,
  form: reduxFormReducer,
});
