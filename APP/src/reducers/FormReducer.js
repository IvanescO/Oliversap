import {
  REGISTRATION_START_SUCCESSFUL,
  REGISTRATION_1STEP_SUCCESSFUL,
  FORM_STORING,
  REGISTRATION_2STEP_SUCCESSFUL,
  REGISTRATION_3STEP_SUCCESSFUL,
  REGISTRATION_4STEP_SUCCESSFUL,
} from "../constants/index";

const initialState = {
  form: {
    EMAIL: "",
    PASSWORD: "",
    PASSWORD_CONFIRM: "",
    LEGAL_FORM: "",
    PHONE: "",
    ADDRESS: "",
    CITY: "",
    STATE: "",
    ZIP: "",
    COUNTRY: "",
    privacy: false,
    phone_number: "",
    phone_code: "",
    country_code: "",
    INDUSTRY: "",
    CEO_ID: "", // TODO:
    REGISTRY_NUMBER: "",
    REGISTRY_CITY: "",
    TAX_NUMBER: "",
    VAT_NUMBER: "",
    LANGUAGE: "",
    SHOP_SYSTEM: "",
    ACQUIRING_BANK: "",
    PSP_SOFTWARE: "",
    WEBSITE: "",
    CONTRACTTYPE_ID: "",
    own_solution: false,
  },
  businessPartner: {},
  address: {},
  contactData: {},
  person: { FIRSTNAME: "", LASTNAME: "" },
  serviceContract: {},
  authLogin: {},
};
function formReducer(state = initialState, action) {
  switch (action.type) {
    case REGISTRATION_START_SUCCESSFUL: {
      const { contactData, authLogin, person } = action;
      return { ...state, contactData, authLogin, person };
    }
    case FORM_STORING: {
      const { form } = action;
      const oldForm = state.form;
      console.log({ ...oldForm, ...form });
      return { ...state, form: { ...oldForm, ...form } };
    }
    case REGISTRATION_1STEP_SUCCESSFUL: {
      const { authLogin, businessPartner, person, address } = action;
      return {
        ...state,
        authLogin,
        businessPartner,
        person,
        address,
      };
    }
    case REGISTRATION_2STEP_SUCCESSFUL: {
      const { serviceContract, businessPartner, person } = action;
      return {
        ...state,
        serviceContract,
        businessPartner,
        person,
      };
    }
    case REGISTRATION_3STEP_SUCCESSFUL: {
      const { serviceContract } = action;
      return {
        ...state,
        serviceContract,
      };
    }
    case REGISTRATION_4STEP_SUCCESSFUL: {
      const { bankDetail, serviceContract, businessPartner } = action;
      return {
        ...state,
        bankDetail,
        serviceContract,
        businessPartner,
      };
    }
    default: {
      return state;
    }
  }
}
export default formReducer;
