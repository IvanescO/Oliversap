import {
  GET_COUNTRY_SUCCESSFUL,
  GET_CONTRACTS_SUCCESSFUL,
  GET_COMPENSATIONS_SUCCESSFUL,
} from "../constants/index";

const initialState = {
  countriesInfo: [],
  contracts: [
    {
      ID: "clear_collect",
      TEXT: "MwSt. einziehen und abführen",
      PRICE: "3% Umsatzkommission",
    },
    { ID: "display", TEXT: "Preisanzeige", PRICE: "EUR 4,95 mtl.*" },
    { ID: "threshold", TEXT: "Lieferschwelle", PRICE: "kostenlos*" },
  ],
  compensation: [
    { ID: "comp_daily", TEXT: "täglich" },
    { ID: "comp_monthly", TEXT: "monatlich" },
    { ID: "comp_weekly", TEXT: "wöchentlich" },
  ],
};
function CountriesInfoReducer(state = initialState, action) {
  switch (action.type) {
    case GET_COUNTRY_SUCCESSFUL: {
      const { countriesInfo } = action;
      countriesInfo.sort(function(a, b) {
        if (a.code > b.code) {
          return 1;
        }
        if (a.code < b.code) {
          return -1;
        }
        return 0;
      });
      console.log(countriesInfo);
      return { ...state, countriesInfo };
    }
    case GET_CONTRACTS_SUCCESSFUL: {
      const { contracts } = action;
      return { ...state, contracts };
    }
    case GET_COMPENSATIONS_SUCCESSFUL: {
      const { compensation } = action;
      return { ...state, compensation };
    }
    default: {
      return state;
    }
  }
}
export default CountriesInfoReducer;
