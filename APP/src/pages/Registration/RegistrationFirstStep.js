import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Grid,
  Col,
  Row,
  FormGroup,
  Radio,
  FormControl,
  DropdownButton,
  MenuItem,
  ButtonGroup,
  Checkbox,
} from "react-bootstrap";
import Wrapper from "../../components/Wrapper";
import { Link } from "react-router-dom";
import {
  GET_COUNTRY,
  CREATE_PERSON,
  REGISTRATION_START,
  REGISTRATION_1STEP,
} from "../../constants/index";
import RegistrationStartForm from "./Forms/RegistrationStartForm";
import RegistrationFirstStepForm from "./Forms/RegistrationFirstStepForm";
import { CountryService } from "../../services/index";

export class RegistrationFirstStep extends Component {
  state = {
    step: "1step",
    countriesInfo: [],
  };

  componentWillMount = async () => {
    let position = await CountryService.getCurrentData();
    this.setState({ position: position.data });
    const { getCountriesInfo } = this.props;
    getCountriesInfo();
  };
  componentWillReceiveProps(nextProps) {
    const {
      CountriesInfoReducer: { countriesInfo },
      FormReducer,
    } = nextProps;
    this.setState({ countriesInfo });
  }
  onChangeStep = e => {
    this.setState({ step: e.target.value });
  };


  render() {
    const { form, countriesInfo, position } = this.state;
    return (
      <Wrapper headerColor={"purple"} hideButtons>
        <div>
          <div className="reg">
            <Grid>
              <div className="wrapper">
                <div className="radio-block">
                  <FormGroup className="clearfix">
                    <Radio
                      className={this.state.step === "1step" && "active-circle"}
                      name="radioGroup"
                      value="1step"
                      defaultChecked

                    >
                      Basics
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "2step" && "active-circle"}
                      value="2step"

                    >
                      Geschäftsinfos
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "3step" && "active-circle"}
                      value="3step"

                    >
                      Vertrag
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "4step" && "active-circle"}
                      value="4step"

                    >
                      Kontodatails
                    </Radio>
                  </FormGroup>
                </div>

                <div className="login-block">
                  <h2>Geschäftskonto eröffnen</h2>
                  <p>Log-in Daten festlegen</p>
                  <RegistrationFirstStepForm
                    countriesInfo={countriesInfo}
                    position={position}
                  />
                </div>
              </div>
            </Grid>
          </div>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = ({ CountriesInfoReducer, FormReducer }) => ({
  CountriesInfoReducer,
  FormReducer,
});

const mapDispatchToProps = dispatch => {
  return {
    getCountriesInfo: () => {
      dispatch({ type: GET_COUNTRY });
    },
    firstStepComplete: form => {
      dispatch({ type: REGISTRATION_1STEP, form });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationFirstStep);
