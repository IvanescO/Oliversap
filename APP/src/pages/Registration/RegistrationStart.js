import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Col,
  Row,
  Grid,
  FormGroup,
  FormControl,
  ControlLabel,
} from "react-bootstrap";
import Wrapper from "../../components/Wrapper";
import { Link } from "react-router-dom";
import { REGISTRATION_START } from "../../constants/index";
import RegistrationStartForm from "./Forms/RegistrationStartForm";

export class RegistrationStart extends Component {
  render() {
    return (
      <Wrapper headerColor={"purple"} hideButtons>
        <div>
          <div className="reg reg-start">
            <Grid>
              <div className="main-block align-center">
                <p className="reg-heading">Geschäftskonto eröffnen</p>
                <p className="sub-heading">
                  Bitte geben Sie Ihre emailadresse an
                </p>
                <RegistrationStartForm />
              </div>
            </Grid>
          </div>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationStart);
