import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Grid,
  Col,
  Row,
  FormGroup,
  Radio,
  FormControl,
  Checkbox,
} from "react-bootstrap";
import Wrapper from "../../components/Wrapper";
import { Link } from "react-router-dom";
import { REGISTRATION_2STEP } from "../../constants/index";
import RegistrationSecondStepForm from "./Forms/RegistrationSecondStepForm";

export class RegistrationSecondtStep extends Component {
  state = {
    step: "2step",
  };

  render() {
    return (
      <Wrapper headerColor={"purple"} hideButtons>
        <div>
          <div className="reg second-step">
            <Grid>
              <div className="wrapper">
                <div className="radio-block">
                  <FormGroup className="clearfix">
                    <Radio
                      className={this.state.step === "1step" && "active-circle"}
                      name="radioGroup"
                      value="1step"
                      // checked={this.state.step === "1step"}
                      // onChange={this.onChangeStep}
                    >
                      Basics
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "2step" && "active-circle"}
                      value="2step"
                      // onChange={this.onChangeStep}
                      // checked={this.state.step === "2step"}
                    >
                      Geschäftsinfos
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "3step" && "active-circle"}
                      value="3step"
                      // onChange={this.onChangeStep}
                      // checked={this.state.step === "3step"}
                    >
                      Vertrag
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "4step" && "active-circle"}
                      value="4step"
                      // onChange={this.onChangeStep}
                      // checked={this.state.step === "4step"}
                    >
                      Kontodatails
                    </Radio>
                  </FormGroup>
                </div>
                <div className="login-block">
                  <h2>Geschäftskonto eröffnen</h2>
                  <p>Information über ihr Geschäft</p>
                  <RegistrationSecondStepForm />
                </div>
              </div>
            </Grid>
          </div>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = ({ CountriesInfoReducer, FormReducer }) => ({
  CountriesInfoReducer,
  FormReducer,
});

const mapDispatchToProps = dispatch => {
  return {
    secondStepComplete: form => {
      dispatch({ type: REGISTRATION_2STEP, form });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationSecondtStep);
