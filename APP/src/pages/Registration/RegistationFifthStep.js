import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Grid,
  Col,
  Row,
  FormGroup,
  Radio,
  FormControl,
  DropdownButton,
  MenuItem,
  ButtonGroup,
  Checkbox,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import Wrapper from "../../components/Wrapper";

export class RegistationFifthStep extends Component {
  state = { step: "2step", form: { own_solution: false } };
  onChangeStep = e => {
    this.setState({ step: e.target.value });
  };
  onSolutionChange = e => {
    const { form } = this.state;
    form["own_solution"] = !form["own_solution"];
    this.setState({ form });
  };
  onFormChange = e => {
    const { form } = this.state;
    form[e.target.name] = e.target.value;
    this.setState({ form });
  };
  render() {
    return (
      <Wrapper headerColor={"purple"} hideButtons>
        <div>
          <div className="reg fifth-step">
            <Grid>
              <div className="wrapper">
                <div className="radio-block">
                  <FormGroup className="clearfix">
                    <Radio
                      className={this.state.step === "1step" && "active-circle"}
                      name="radioGroup"
                      value="1step"

                    >
                      Basics
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "2step" && "active-circle"}
                      value="2step"
                    >
                      Geschäftsinfos
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "3step" && "active-circle"}
                      value="3step"
                    >
                      Vertrag
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "4step" && "active-circle"}
                      value="4step"
                    >
                      Kontodatails
                    </Radio>
                  </FormGroup>
                </div>

                <div className="login-block">
                  <h2>Geschäftskonto eröffnen</h2>
                  <p>Ihre Kontodetails</p>
                  <Row className="margin-bottom">
                    <Col sm={12} className="field">
                      <FormGroup>
                        <FormControl type="text" placeholder="Geschäftsname" />
                      </FormGroup>
                    </Col>
                    <Col sm={12} className="field">
                      <p>Vergütung / Vergütungsfrequenz</p>
                    </Col>
                    <Col sm={12} className="field">
                      <Row>
                        <Col sm={4} className="field compensation">
                          <div>
                            <FormGroup>
                              <Checkbox inline>täglich</Checkbox>
                              <p>5% Umsatzkommission</p>
                            </FormGroup>
                          </div>
                        </Col>
                        <Col sm={4} className="field compensation">
                          <div>
                            <FormGroup>
                              <Checkbox inline>wöchentlich</Checkbox>
                              <p>5% Umsatzkommission</p>
                            </FormGroup>
                          </div>
                        </Col>
                        <Col sm={4} className="field compensation">
                          <div>
                            <FormGroup>
                              <Checkbox inline>monatlich</Checkbox>
                              <p>5% Umsatzkommission</p>
                            </FormGroup>
                          </div>
                        </Col>
                      </Row>
                    </Col>
                    <Col sm={12} className="field">
                      <p>MCC Code</p>
                    </Col>
                    <Col sm={12} className="field">
                      <FormGroup>
                        <FormControl type="text" />
                      </FormGroup>
                    </Col>
                    <Col sm={12} className="field">
                      <p className="footnote">
                        Diesen finden Sie im Akzeptanzvertrag Ihres
                        Payment-Dienstleisters
                      </p>
                    </Col>
                    <Col sm={12} className="field">
                      <p>Kontoangaben / Bankkonto</p>
                    </Col>
                    <Col sm={8} className="field">
                      <FormGroup>
                        <FormControl type="text" placeholder="Name der Bank" />
                      </FormGroup>
                    </Col>
                    <Col
                      sm={4}
                      className="field"
                      style={{ "padding-left": "0" }}
                    >
                      <FormGroup>
                        <FormControl type="text" placeholder="Land" />
                      </FormGroup>
                    </Col>
                    <Col sm={12} className="field">
                      <FormGroup>
                        <FormControl type="text" placeholder="IBAN" />
                      </FormGroup>
                    </Col>
                    <Col sm={12}>
                      <Col sm={6} className="field" style={{ padding: "0" }}>
                        <Col sm={12} className="field" style={{ padding: "0" }}>
                          <FormGroup>
                            <FormControl type="text" placeholder="IBAN" />
                          </FormGroup>
                        </Col>
                        <Col sm={12} className="field" style={{ padding: "0" }}>
                          <FormGroup>
                            <FormControl type="text" placeholder="IBAN" />
                          </FormGroup>
                        </Col>
                      </Col>
                      <Col sm={6} className="field" style={{ padding: "0" }}>
                        <Col sm={12} style={{ "padding-right": "0" }}>
                          <div
                            className="compensation"
                            style={{ width: "100%" }}
                          >
                            <FormGroup
                              style={{ padding: "12px 15px", height: "95px" }}
                            >
                              <Checkbox inline>
                                Bitte belasten Sie Gebühren per ELV
                              </Checkbox>

                              <p>
                                Bdi duntius solores ducia vit ex enisqui
                                conectatus arcid millabo rererum.
                              </p>
                            </FormGroup>
                          </div>
                        </Col>
                      </Col>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={12} className="field">
                      <div className="align-center">
                        <button className="blue-bg">LET‘S GO</button>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </Grid>
          </div>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(
  RegistationFifthStep
);
