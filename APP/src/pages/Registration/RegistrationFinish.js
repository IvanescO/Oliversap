import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid } from "react-bootstrap";
import Wrapper from "../../components/Wrapper";

export class RegistrationFinish extends Component {
  // Finally open the ClearVAT Homepage
  handleClick() {
    window.open("http://clearvat.com", "_self");
  };
  render() {
    return (
      <Wrapper headerColor={"purple"} hideButtons>
        <div className="reg finish-reg">
          <Grid>
            <div className="main-block">
              <h2 className="align-center">Willkommen bei ClearVAT</h2>
              <p className="reg-heading">Das ist jetzt zu tun:</p>
              <ol>
                <li>
                  Sie erhalten eine Bestätigungsemail. Bitte verifizieren Sie
                  zunächst Ihren Account.
                </li>
                <li>
                  Drucken Sie den beigefügten Vertragsentwurf aus und senden Sie
                  diesen unterzeichnet an ClearVAT.
                </li>
                <li>
                  Drucken Sie das SEPA Lastschrift Mandat für wiederkehrende
                  Lastschriften aus und fügen sie es unterzeichnet dem Vertrag
                  bei.
                </li>
                <li>
                  Loggen Sie sich – nach Verifizierung – in Ihren User Bereich
                  ein und erkunden Sie die Funktionen.
                </li>
              </ol>
              <div className="align-center">
                <button onClick={this.handleClick} className="blue-bg">
                  OK, VERSTANDEN
                </button>
              </div>
            </div>
          </Grid>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationFinish);
