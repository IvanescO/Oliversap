import RegistrationStart from "./RegistrationStart";
import RegistrationFirstStep from "./RegistrationFirstStep";
import RegistrationSecondtStep from "./RegistrationSecondtStep";
import RegistationFourthStep from "./RegistationFourthStep";
import RegistrationFinish from "./RegistrationFinish";
import RegistationThirdStep from "./RegistationThirdStep";

export {
  RegistrationStart,
  RegistrationFirstStep,
  RegistrationSecondtStep,
  RegistationThirdStep,
  RegistationFourthStep,
  RegistrationFinish,
};
