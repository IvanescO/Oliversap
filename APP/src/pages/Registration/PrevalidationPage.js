import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Col,
  Row,
  Grid,
  FormGroup,
  FormControl,
  ControlLabel,
} from "react-bootstrap";
import Wrapper from "../../components/Wrapper";
import { Link, withRouter } from "react-router-dom";
import { REGISTRATION_START, REGISTRATION_4STEP } from "../../constants/index";
import RegistrationStartForm from "./Forms/RegistrationStartForm";

class PreValidationPage extends Component {
  render() {
    const { form } = this.props;
    console.log("form");
    console.log(form);
    return (
      <Wrapper headerColor={"purple"} hideButtons>
        <div>
          <div className="reg reg-start">
            <Grid>
              <div className="main-block align-center">
                {Object.keys(form).map(key => {
                  console.log(key);
                  return (
                    <div key={key}>
                      {key}: {form[key]}
                    </div>
                  );
                })}
                <Link to="/registration-finish">
                  <button onClick={() => {}} className="blue-border">
                    weiter
                  </button>
                </Link>
              </div>
            </Grid>
          </div>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  const form = state.FormReducer.form;
  delete form.own_solution;
  return {
    form,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fourthStepComplete: ({ form, router }) => {
      dispatch({ type: REGISTRATION_4STEP, form, router });
    },
    dispatch,
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PreValidationPage)
);
