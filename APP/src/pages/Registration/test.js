import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Grid,
  Form,
  Col,
  Row,
  FormGroup,
  Radio,
  FormControl,
  InputGroup,
  DropdownButton,
  MenuItem,
  Button,
  Checkbox,
  ControlLabel,
  Tab,
  Tabs,
  NavItem,
  Nav,
} from "react-bootstrap";
import { ButtonGroup } from "react-bootstrap";
import Wrapper from "../../components/Wrapper";

export class RegistationFourthStep extends Component {
  state = { step: "4step", form: { own_solution: false } };
  onChangeStep = e => {
    this.setState({ step: e.target.value });
  };
  handleSelect = eventKey => {
    alert(`selected ${eventKey}`);
  };
  onSolutionChange = e => {
    const { form } = this.state;
    form[e.target.name] = !form[e.target.name];
    this.setState({ form });
  };
  onFormChange = e => {
    const { form } = this.state;
    form[e.target.name] = e.target.value;
    this.setState({ form });
  };
  render() {
    return (
      <Wrapper headerColor={"purple"}>
        <div>
          <div className="reg fourth-step">
            <Grid>
              <div className="wrapper">
                <div className="radio-block">
                  <FormGroup className="clearfix">
                    <Radio
                      className={this.state.step === "1step" && "active-circle"}
                      name="radioGroup"
                      value="1step"
                      //checked={this.state.step === "1step"}
                      //   onChange={this.onChangeStep}
                    >
                      Basics
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "2step" && "active-circle"}
                      value="2step"
                      //   onChange={this.onChangeStep}
                      //checked={this.state.step === "2step"}
                    >
                      Geschäftsinfos
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "3step" && "active-circle"}
                      value="3step"
                      //   onChange={this.onChangeStep}
                      //checked={this.state.step === "3step"}
                    >
                      Vertrag
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "4step" && "active-circle"}
                      value="4step"
                      //   onChange={this.onChangeStep}
                      //checked={this.state.step === "4step"}
                    >
                      Kontodatails
                    </Radio>
                  </FormGroup>
                </div>

                <div className="login-block">
                  <h2>Geschäftskonto eröffnen</h2>
                  <p>Vertrag beantragen</p>
                  <Row>
                    <Col sm={12}>
                      <FormGroup>
                        <FormControl
                          type="text"
                          placeholder=" Webseite / www."
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <p>Wählen Sie Leistungen von ClearVAT aus:</p>

                  <Col sm={12}>
                    <Tab.Container
                      id="left-tabs-example"
                      defaultActiveKey="first"
                    >
                      <Row className="clearfix">
                        <Col sm={12} className="tabs-block">
                          <Nav
                            bsStyle="pills"
                            className="row tabs-nav clearfix"
                            stacked
                            onSelect={k => this.handleSelect(k)}
                          >
                            <NavItem eventKey="first" className="col-md-4 col-sm-6">
                              <span className="active-state" />
                              <div className="tabs-name">
                                <span>SHOP PLUG-IN</span>
                              </div>
                              <div className="tabs-price">free*</div>
                            </NavItem>
                            <NavItem eventKey="second" className="col-md-4 col-sm-6">
                              <span className="active-state" />
                              <div className="tabs-name">
                                <span>DISPLAY</span>
                              </div>
                              <div className="tabs-price">EUR 4,95/mtl*</div>
                            </NavItem>
                            <NavItem eventKey="third" className="col-md-4 col-sm-6">
                              <span className="active-state" />
                              <div className="tabs-name">
                                <span>COLLECT + CLEAR</span>
                              </div>
                              <div className="tabs-price">3% Umsatz</div>
                            </NavItem>
                          </Nav>
                        </Col>
                        <p className="footnote">
                          * Nur, wenn SIe im Zielland den Grenzwert
                          (Threshold) überschreiten (autmatisch)
                        </p>
                        <Col sm={12} className="tabs-list">
                          <Tab.Content animation>
                            <Tab.Pane eventKey="first">
                              <ul>
                                <li>
                                  Auswertungen und Statistiken zu Ihren
                                  Auslands- umsätzen (sales stats)
                                </li>
                                <li>
                                  Zugriff auf die ClearVAT SKU-Datenbank mit
                                  Bildern (über 1 Mio Artikel)
                                </li>
                              </ul>
                              <p className="footnote">* Bei Störungen berechnen wir eine Service Fee.</p>
                            </Tab.Pane>
                            <Tab.Pane eventKey="second">
                              <ul>
                                <li>
                                  Auswertungen und Statistiken zu Ihren
                                  Auslands- umsätzen (sales stats)
                                </li>
                                <li>
                                  Zugriff auf die ClearVAT SKU-Datenbank mit
                                  Bildern (über 1 Mio Artikel)
                                </li>
                              </ul>
                              <p className="footnote">* Bei Störungen berechnen wir eine Service Fee.</p>
                            </Tab.Pane>
                            <Tab.Pane eventKey="third">
                              <ul>
                                <li>
                                  Auswertungen und Statistiken zu Ihren
                                  Auslands- umsätzen (sales stats)
                                </li>
                                <li>
                                  Zugriff auf die ClearVAT SKU-Datenbank mit
                                  Bildern (über 1 Mio Artikel)
                                </li>
                              </ul>
                              <p className="footnote">* Bei Störungen berechnen wir eine Service Fee.</p>
                            </Tab.Pane>
                          </Tab.Content>
                        </Col>
                      </Row>
                    </Tab.Container>
                  </Col>
                  <Row>
                    <Col sm={12} className="field">
                      <div className="align-center">
                        <button className="blue-bg">weiter</button>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </Grid>
          </div>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(
  RegistationFourthStep
);
