import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import RenderField from './RenderField';
import RenderLangCheckBox from './RenderLangCheckBox';
import RenderTextCheckBox from './RenderTextCheckBox';
import validate from './validateSecondStepFields';
import { REGISTRATION_2STEP } from '../../../constants/index';

const RegSecondForm = props => {
  const {
    handleSubmit,
    // pristine,
    // reset,
    // submitting,
    dispatch,
    secondStepComplete
  } = props;

  // const form = {};

  let submit = form => {
    const router = props.history;
    secondStepComplete({ form, router });
  };

  return (
    <div>
      <Row className="margin-bottom">
        <Col sm={6}>
          <Field
            name="FIRSTNAME"
            label="Vorname"
            id="FIRSTNAME_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={6}>
          <Field
            name="LASTNAME"
            label="Nachname"
            id="LASTNAME_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12}>
          <Field
            name="NAME"
            label="Geschäftsname"
            id="NAME_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12}>
          <Field
            name="INDUSTRY"
            label="Branche"
            id="INDUSTRY_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12}>
          <Field
            name="WEBSITE"
            label=" Webseite / www."
            id="WEBSITE_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12} className="check-language">
          <span className="form-group">Sprache</span>
          <Field
            name="LANGUAGE"
            text="deutsch"
            dispatchFunc={dispatch}
            languageCode="DE"
            id="LANGUAGE_ID"
            component={RenderLangCheckBox}
          />
          <Field
            name="LANGUAGE"
            text="english"
            dispatchFunc={dispatch}
            languageCode="EN"
            id="LANGUAGE_ID"
            component={RenderLangCheckBox}
          />
        </Col>
        <Col sm={12}>
          <Field
            name="CEO_ID"
            label=" Rechtlicher Vertreter (GF)"
            id="CEO_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12} styles={{ position: 'relative' }}>
          <div className="left-block">
            <Field
              name="REGISTRY_NUMBER"
              label="HRB Nummer"
              id="REGISTRY_NUMBER_ID"
              component={RenderField}
            />
          </div>
          <span className="text-between">in</span>
          <div className="right-block">
            <Field
              name="REGISTRY_CITY"
              label="Stadt"
              id="REGISTRY_CITY_ID"
              component={RenderField}
            />
          </div>
        </Col>
        <Col sm={6}>
          <Field
            name="TAX_NUMBER"
            label="Steuernummer"
            id="TAX_NUMBER_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={6}>
          <Field
            name="VAT_NUMBER"
            label="USt-Nr"
            id="VAT_NUMBER_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={8}>
          <Field
            name="SHOP_SYSTEM"
            label="Shopsystem (Magento, XT-Commerce)"
            id="SHOP_SYSTEM_ID"
            component={RenderField}
          />
        </Col>
        <Col
          sm={4}
          onClick={this.onSolutionChange}
          style={{ cursor: 'pointer' }}>
          <Field
            name="own_solution"
            text="Eigene Lösung"
            id="own_solution_id"
            dispatchFunc={dispatch}
            component={RenderTextCheckBox}
          />
        </Col>
        <Col sm={6}>
          <Field
            name="ACQUIRING_BANK"
            label="Acquiring Bank"
            id="ACQUIRING_BANK_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={6}>
          <Field
            name="PSP_SOFTWARE"
            label="PSP Software"
            id="PSP_SOFTWARE_ID"
            component={RenderField}
          />
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <div className="align-center">
            <button onClick={handleSubmit(submit)} className="blue-bg">
              weiter
            </button>
          </div>
        </Col>
      </Row>
    </div>
  );
};

const withReduxForm = reduxForm({
  form: 'regSecondForm',
  validate // a unique identifier for this form
})(RegSecondForm);

const mapStateToProps = state => {
  const form = state.FormReducer.form;

  return {
    initialValues: {
      FIRSTNAME: form.FIRSTNAME,
      LASTNAME: form.LASTNAME,
      NAME: form.NAME,
      WEBSITE: form.WEBSITE,
      LANGUAGE: form.LANGUAGE || 'DE', //FIXME: CHANGE TO COMPUTE VALUE
      INDUSTRY: form.INDUSTRY,
      CEO_ID: form.CEO_ID, // TODO:
      REGISTRY_NUMBER: form.REGISTRY_NUMBER,
      REGISTRY_CITY: form.REGISTRY_CITY,
      TAX_NUMBER: form.TAX_NUMBER,
      VAT_NUMBER: form.VAT_NUMBER,
      SHOP_SYSTEM: form.SHOP_SYSTEM,
      ACQUIRING_BANK: form.ACQUIRING_BANK,
      PSP_SOFTWARE: form.PSP_SOFTWARE,
      own_solution: form.own_solution
    }
  };
};

const mapDispatchToProps = dispatch => {
  return {
    secondStepComplete: ({ form, router }) => {
      dispatch({ type: REGISTRATION_2STEP, form, router });
    },
    dispatch
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withReduxForm)
);
