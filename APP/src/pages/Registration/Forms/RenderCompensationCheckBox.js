import React from 'react';
import { Checkbox } from 'react-bootstrap';

const RenderCompensationCheckBox = ({
  input,
  text,
  chekBoxValue,
  meta: { touched, error, warning }
}) => {
  return (
    <Checkbox
      // {...input}
      inline
      value={chekBoxValue}
      checked={input.value === chekBoxValue}
      className={input.value === chekBoxValue ? 'check active-box' : 'check'}>
      {text}
    </Checkbox>
  );
};

export default RenderCompensationCheckBox;
