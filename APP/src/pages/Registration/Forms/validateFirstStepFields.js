const validate = values => {
  const errors = {};

  if (!values.EMAIL) {
    errors.EMAIL = "Pflichtfeld";
  } else if (
    !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      values.EMAIL
    )
  ) {
    //errors.EMAIL = "Invalid email address";
	  errors.EMAIL = "Ungültige email Addresse";
  }
  if (!values.PASSWORD) {
    errors.PASSWORD = "Pflichtfeld";
  }
  if (values.PASSWORD) {
    const reg = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g;
    if (values.PASSWORD.length < 8 || !reg.test(values.PASSWORD)) {
      //errors.PASSWORD = "Invalid";
    	errors.PASSWORD = "Ungültiges Passwort";
    }
  }

  if (!values.PASSWORD_CONFIRM) {
    errors.PASSWORD_CONFIRM = "Pflichtfeld";
  }
  if (values.PASSWORD_CONFIRM) {
    if (values.PASSWORD_CONFIRM != values.PASSWORD) {
      //errors.PASSWORD_CONFIRM = "DOESN'T MATCH";
    	errors.PASSWORD_CONFIRM = "Passworte stimmen nicht überein";
    }
  }
  if (!values.FIRSTNAME) {
    errors.FIRSTNAME = "Pflichtfeld";
  }
  if (!values.LASTNAME) {
    errors.LASTNAME = "Pflichtfeld";
  }
  if (!values.NAME) {
    errors.NAME = "Pflichtfeld";
  }
  if (!values.LEGAL_FORM) {
    errors.LEGAL_FORM = "Pflichtfeld";
  }
  if (!values.CITY) {
    errors.CITY = "Pflichtfeld";
  }
  if (!values.ADDRESS) {
    errors.ADDRESS = "Pflichtfeld";
  }
  if (!values.ZIP) {
    errors.ZIP = "Pflichtfeld";
  }
  if (!values.phone_number) {
    errors.phone_number = "Pflichtfeld";
  }
  if (!values.phone_number) {
    errors.phone_number = "Pflichtfeld";
  }
  if (!/^\d+$/.test(values.phone_number)) {
    //errors.phone_number = "Invalid";
	  errors.phone_number = "Ungültige Telefonnummer";
  }
  if (!values.phone_code) {
    errors.phone_code = "Pflichtfeld";
  }
  if (!values.COUNTRY) {
    errors.COUNTRY = "Pflichtfeld";
  }
  if (!values.PRIVACYUSE) {
    errors.PRIVACYUSE = "Pflichtfeld";
  }
  return errors;
};

export default validate;
