import React from 'react';
import { change } from 'redux-form';
import { FormGroup, Checkbox } from 'react-bootstrap';

const RenderLangCheckBox = ({ input, text, languageCode, dispatchFunc }) => {
  return (
    <FormGroup
      onClick={e => {
        dispatchFunc(change('regSecondForm', input.name, languageCode));
      }}
      style={{ cursor: 'pointer' }}>
      <Checkbox
        inline
        name={input.name}
        value={languageCode}
        checked={input.value === languageCode}
        className={input.value === languageCode ? 'check active-box' : 'check'}
        onChange={() => {}}
      />
      <span>{text}</span>
    </FormGroup>
  );
};

export default RenderLangCheckBox;
