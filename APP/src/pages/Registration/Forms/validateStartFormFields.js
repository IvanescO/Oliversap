const validate = values => {
  const errors = {};

  if (!values.email) {
    //errors.email = 'Required';
    errors.email = "Pflichtfeld";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    //errors.email = 'Invalid email address';
    errors.email = "Ungültige email Addresse";
  }
  return errors;
};

export default validate;
