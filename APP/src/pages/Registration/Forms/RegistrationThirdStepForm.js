import React from 'react';
import { Field, reduxForm, change } from 'redux-form';
import { Row, Col, Tab, Nav, NavItem } from 'react-bootstrap';
import { REGISTRATION_3STEP } from '../../../constants/index';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import RenderField from './RenderField';
import validate from './validateThirdFormFields';

const RegistrationThirdStepForm = props => {
  console.log(props);
  const {
    handleSubmit,
    contracts,
    // pristine,
    // reset,
    // submitting,
    dispatch,
    secondThirdComplete
  } = props;

  let submit = form => {
    const router = props.history;

    if (!form.CONTRACTTYPE_ID) return;
    secondThirdComplete({ form, router });
  };

  let selectContractType = value => {
    dispatch(change('RegistrationThirdStepForm', 'CONTRACTTYPE_ID', value));
  };

  return (
    <div>
      <Row>
        <Col sm={12} className="field">
          <Field
            name="WEBSITE"
            label="Webseite / www."
            id="WEBSITE_ID"
            component={RenderField}
          />
        </Col>
      </Row>
      <p>Wählen Sie Leistungen von ClearVAT aus:</p>

      <Col sm={12} className="field">
        <Tab.Container
          id="left-tabs-example"
          defaultActiveKey={props.initialValues.CONTRACTTYPE_ID}>
          <Row className="clearfix">
            <Col sm={12} className="tabs-block">
              <Nav
                bsStyle="pills"
                className="row tabs-nav clearfix"
                stacked
                onSelect={selectContractType}>
                <NavItem eventKey={contracts[2].ID} className="col-sm-4">
                  <span className="active-state" />
                  <div className="tabs-name">
                    <span>{contracts[2].TEXT}</span>
                  </div>
                  <div className="tabs-price">{contracts[2].PRICE}</div>
                </NavItem>
                <NavItem eventKey={contracts[1].ID} className="col-sm-4">
                  <span className="active-state" />
                  <div className="tabs-name">
                    <span>{contracts[1].TEXT}</span>
                  </div>
                  <div className="tabs-price">{contracts[1].PRICE}</div>
                </NavItem>
                <NavItem eventKey={contracts[0].ID} className="col-sm-4">
                  <span className="active-state" />
                  <div className="tabs-name">
                    <span>{contracts[0].TEXT}</span>
                  </div>
                  <div className="tabs-price">{contracts[0].PRICE}</div>
                </NavItem>
              </Nav>
            </Col>
            <p className="footnote">
              * Nur, wenn SIe im Zielland den Grenzwert (Threshold)
              überschreiten (autmatisch)
            </p>
            <Col sm={12} className="tabs-list">
              <Tab.Content animation>
                <Tab.Pane eventKey={contracts[0].ID}>
                  <ul>
                    <li>
                      Auswertungen und Statistiken zu Ihren Auslands- umsätzen
                      (sales stats)
                    </li>
                    <li>
                      Zugriff auf die ClearVAT SKU-Datenbank mit Bildern (über 1
                      Mio Artikel)
                    </li>
                  </ul>
                  <p className="footnote">
                    * Bei Störungen berechnen wir eine Service Fee.
                  </p>
                </Tab.Pane>
                <Tab.Pane eventKey={contracts[1].ID}>
                  <ul>
                    <li>
                      Auswertungen und Statistiken zu Ihren Auslands- umsätzen
                      (sales stats)
                    </li>
                    <li>
                      Zugriff auf die ClearVAT SKU-Datenbank mit Bildern (über 1
                      Mio Artikel)
                    </li>
                  </ul>
                  <p className="footnote">
                    * Bei Störungen berechnen wir eine Service Fee.
                  </p>
                </Tab.Pane>
                <Tab.Pane eventKey={contracts[2].ID}>
                  <ul>
                    <li>
                      Auswertungen und Statistiken zu Ihren Auslands- umsätzen
                      (sales stats)
                    </li>
                    <li>
                      Zugriff auf die ClearVAT SKU-Datenbank mit Bildern (über 1
                      Mio Artikel)
                    </li>
                  </ul>
                  <p className="footnote">
                    * Bei Störungen berechnen wir eine Service Fee.
                  </p>
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </Col>
      <Row>
        <Col
          style={{
            justifyContent: 'center',
            display: 'flex'
          }}
          sm={12}>
          <button onClick={handleSubmit(submit)} className="blue-bg">
            weiter
          </button>
        </Col>
      </Row>
    </div>
  );
};

const withReduxForm = reduxForm({
  form: 'RegistrationThirdStepForm',
  validate // a unique identifier for this form
})(RegistrationThirdStepForm);

const mapStateToProps = state => {
  const form = state.FormReducer.form;
  // console.log(form.CONTRACTTYPE_ID);

  return {
    initialValues: {
      WEBSITE: form.WEBSITE,
      CONTRACTTYPE_ID: form.CONTRACTTYPE_ID
    }
  };
};

const mapDispatchToProps = dispatch => {
  return {
    secondThirdComplete: ({ form, router }) => {
      dispatch({ type: REGISTRATION_3STEP, form, router });
    },
    dispatch
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withReduxForm)
);
