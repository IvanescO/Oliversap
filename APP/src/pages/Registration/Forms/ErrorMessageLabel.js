import React, { Fragment } from "react";
import { ControlLabel } from "react-bootstrap";

const ErrorMessageLabel = ({ errorText, requiredField, errorColor }) => (
  <Fragment>
    {
      requiredField ?
        <ControlLabel
          className="form-error"
          style={{ color: errorColor }}
        //> Validierungsfehler: {errorText.toLowerCase()}
      	> Validierungsfehler: {errorText}
        </ControlLabel> : null
    }
  </Fragment>
);

export default ErrorMessageLabel;
