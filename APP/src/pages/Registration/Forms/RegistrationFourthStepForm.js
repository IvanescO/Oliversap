import React from "react";
import { Field, reduxForm, change } from "redux-form";
import { FormGroup, Col, Row } from "react-bootstrap";
import { REGISTRATION_4STEP } from "../../../constants/index";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import RenderField from "./RenderField";
import RenderCompensationCheckBox from "./RenderCompensationCheckBox";
import RenderCountryField from "./RenderCountryField";
import RenderDebitCheckBox from "./RenderDebitCheckBox";
import validate from "./validateFouthFormFields";

const RegistrationFourthStepForm = props => {
  const {
    handleSubmit,
    // contracts,
    // pristine,
    // reset,
    // submitting,
    dispatch,
    countriesInfo,
    compensation,
    fourthStepComplete,
  } = props;

  let submit = form => {
    const router = props.history;
    fourthStepComplete({ form, router });
  };

  let selectContractType = value => {
    dispatch(change("RegistrationFourthStepForm", "CONTRACTTYPE_ID", value));
  };

  return (
    <div>
      <Row className="margin-bottom">
        <Col sm={12}>
          <Field
            name="NAME"
            label="Geschäftsname"
            id="NAME_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12}>
          <p>Vergütung / Vergütungsfrequenz</p>
        </Col>
        <Col sm={12}>
          <Row>
            <Col sm={4} className="compensation">
              <div>
                <FormGroup
                  onClick={() =>
                    dispatch(
                      change(
                        "RegistrationFourthStepForm",
                        "COMPENSATION_ID",
                        compensation[0].ID
                      )
                    )
                  }
                  style={{ marginBottom: "0" }}
                >
                  <Field
                    chekBoxValue={compensation[0].ID}
                    name="COMPENSATION_ID"
                    text={compensation[0].TEXT}
                    component={RenderCompensationCheckBox}
                  />

                  <p>5% Umsatzkommission</p>
                </FormGroup>
              </div>
            </Col>
            <Col sm={4} className="compensation">
              <div>
                <FormGroup
                  style={{ marginBottom: "0" }}
                  onClick={() =>
                    dispatch(
                      change(
                        "RegistrationFourthStepForm",
                        "COMPENSATION_ID",
                        compensation[2].ID
                      )
                    )
                  }
                >
                  <Field
                    chekBoxValue={compensation[2].ID}
                    name="COMPENSATION_ID"
                    text={compensation[2].TEXT}
                    component={RenderCompensationCheckBox}
                  />

                  <p>4% Umsatzkommission</p>
                </FormGroup>
              </div>
            </Col>
            <Col sm={4} className="compensation">
              <div>
                <FormGroup
                  onClick={() =>
                    dispatch(
                      change(
                        "RegistrationFourthStepForm",
                        "COMPENSATION_ID",
                        compensation[1].ID
                      )
                    )
                  }
                  style={{ marginBottom: "0" }}
                >
                  <Field
                    chekBoxValue={compensation[1].ID}
                    name="COMPENSATION_ID"
                    text={compensation[1].TEXT}
                    component={RenderCompensationCheckBox}
                  />

                  <p>3% Umsatzkommission</p>
                </FormGroup>
              </div>
            </Col>
          </Row>
        </Col>
        <Col sm={12}>
          <p>MCC Code</p>
        </Col>
        <Col sm={12}>
          <Field
            name="MCC_CODE"
            label="Code"
            id="MCC_CODE_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12}>
          <p className="footnote">
            Diesen finden Sie im Akzeptanzvertrag Ihres Payment-Dienstleisters
          </p>
        </Col>
        <Col sm={12}>
          <p>Kontoangaben / Bankkonto</p>
        </Col>
        <Col sm={8}>
          <Field
            name="BANK_NAME"
            label="Name der Bank"
            id="BANK_NAME_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={4} className="formgroup-double">
          <FormGroup>
            <Field
              name="BANK_COUNTRY"
              data={countriesInfo}
              labelKey={"code"}
              borderRight={false}
              label="Country code"
              id="COUNTRY_ID"
              component={RenderCountryField}
            />
          </FormGroup>
        </Col>
        <Col sm={12}>
          <Field
            name="IBAN"
            label="IBAN"
            id="IBAN_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12}>
          <Col sm={6} style={{ padding: "0" }}>
            <Col sm={12} style={{ padding: "0" }}>
              <Field
                name="SWIFT"
                label="SWIFT / BIS"
                id="SWIFT_ID"
                component={RenderField}
              />
            </Col>
            <Col sm={12} style={{ padding: "0" }}>
              <Field
                name="ACCOUNTOWNER"
                label="Kontoinhaber"
                id="ACCOUNTOWNER_ID"
                component={RenderField}
              />
            </Col>
          </Col>
          <Col sm={6} style={{ padding: "0" }}>
            <Col sm={12} style={{ paddingRight: "0" }} className="checkbox-lg">
              <div className="compensation" style={{ width: "100%" }}>
                <FormGroup
                  style={{
                    padding: "8px 15px 0",
                    minHeight: "95px",
                  }}
                >
                  <Field
                    name="DEBIT_AUTHORIZATION"
                    component={RenderDebitCheckBox}
                    text={" Bitte belasten Sie Gebühren per ELV"}
                  />
                  <p style={{ fontSize: "10px" }}>
                    Bdi duntius solores ducia vit ex enisqui conectatus arcid
                    millabo rererum.
                  </p>
                </FormGroup>
              </div>
            </Col>
          </Col>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <div className="align-center">
            <Link to="/registration-finish">
              <button onClick={handleSubmit(submit)} className="blue-bg">
                LET‘S GO
              </button>
            </Link>
          </div>
        </Col>
      </Row>
    </div>
  );
};

const withReduxForm = reduxForm({
  form: "RegistrationFourthStepForm",
  validate, // a unique identifier for this form
})(RegistrationFourthStepForm);

const mapStateToProps = state => {
  const form = state.FormReducer.form;

  return {
    initialValues: {
      NAME: form.NAME,
      WEBSITE: form.WEBSITE,
      COUNTRY: form.COUNTRY,
      ACCOUNTOWNER: `${form.FIRSTNAME || ""} ${form.LASTNAME || ""}`,

      DEBIT_AUTHORIZATION: form.DEBIT_AUTHORIZATION,
      COMPENSATION_ID: form.COMPENSATION_ID,
      MCC_CODE: form.MCC_CODE,
      BANK_NAME: form.BANK_NAME, //TODO: ment it
      BANK_COUNTRY: form.BANK_COUNTRY || form.COUNTRY,
      IBAN: form.IBAN,
      SWIFT: form.SWIFT,
    },
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fourthStepComplete: ({ form, router }) => {
      dispatch({ type: REGISTRATION_4STEP, form, router });
    },
    dispatch,
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withReduxForm)
);
