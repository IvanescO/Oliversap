import React from 'react';
import { FormGroup, Checkbox } from 'react-bootstrap';

const RenderTextCheckBox = ({ input }) => {
  return (
    <FormGroup className="eigene">
      <Checkbox
        {...input}
        inline
        checked={input.value}
        className={input.value ? 'check active-box' : 'check'}>
        Eigene Lösung
      </Checkbox>
    </FormGroup>
  );
};

export default RenderTextCheckBox;
