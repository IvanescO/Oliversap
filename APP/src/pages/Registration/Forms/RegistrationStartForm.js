import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Col } from 'react-bootstrap';
import { REGISTRATION_START } from '../../../constants/index';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import RenderField from './RenderField';
import validate from './validateStartFormFields';
import formColors from '../../../utils/formColors';

const RegStartForm = props => {
  const {
    handleSubmit,
    // pristine,
    // reset,
    // submitting,
    registrationStart
  } = props;

  let submit = ({ email }) => {
    const router = props.history;

    registrationStart({ email, router });
  };

  return (
    <div>
      <Field
        name="email"
        type="email"
        id="email_id"
        label="Email Adresse"
        className="form-control mail"
        component={RenderField}
      />
      <button onClick={handleSubmit(submit)} className="blue-border">
        weiter
      </button>
    </div>
  );
};

const withReduxForm = reduxForm({
  form: 'regStartForm',
  validate // a unique identifier for this form
})(RegStartForm);

const mapStateToProps = state => {
  const form = state.FormReducer.form;
  return {
    initialValues: { email: form.EMAIL }
  };
};

const mapDispatchToProps = dispatch => {
  return {
    registrationStart: ({ email, router }) => {
      dispatch({ type: REGISTRATION_START, email, router });
    }
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withReduxForm)
);
