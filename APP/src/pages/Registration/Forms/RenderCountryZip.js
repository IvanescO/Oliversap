import React from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';
import { FormGroup, FormControl } from 'react-bootstrap';
import ErrorMessageLabel from './ErrorMessageLabel';
import formColors from '../../../utils/formColors';

const { ERROR_COLOR } = formColors;

const RenderCountryZip = ({
  labelKey,
  borderRight,
  data,
  label,
  id,
  className,
  COUNTRY,
  ZIP
}) => {
  const countryError = COUNTRY.meta.touched && COUNTRY.meta.error;
  const zipError = ZIP.meta.touched && ZIP.meta.error;
  const errorText = COUNTRY.meta.error || ZIP.meta.error;
  const isRequiredField = countryError || zipError;
  const isSelectId = id[0] || id[1];
  data = data.filter(e => e.dial_code);

  return (
    <FormGroup
      controlId={isSelectId}
      validationState={isRequiredField ? 'error' : null}
      style={{ display: 'flex', flexWrap: 'wrap' }}
      className="double-drop">
      <div style={{ width: '40%' }}>
        <Typeahead
          {...COUNTRY.input}
          labelKey={labelKey}
          multiple={false}
          defaultInputValue={COUNTRY.input.value}
          inputProps={{
            style: {
              borderRight: borderRight ? '1 px' : 'none',
              borderTopRightRadius: 0,
              borderBottomRightRadius: 0,
              borderColor: isRequiredField ? ERROR_COLOR : ''
            }
          }}
          options={data}
          placeholder={label[0]}
        />
      </div>
      <FormControl
        {...ZIP.input}
        className={className[0]}
        style={isRequiredField ? { borderColor: ERROR_COLOR } : {}}
        placeholder={label[1]}
      />

      {isRequiredField ? (
        <ErrorMessageLabel
          errorText={errorText}
          errorColor={ERROR_COLOR}
          requiredField={isRequiredField}
        />
      ) : null}
    </FormGroup>
  );
};

export default RenderCountryZip;
