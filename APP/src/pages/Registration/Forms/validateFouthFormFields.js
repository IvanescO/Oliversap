import IBAN from "iban";

const validate = values => {
  const errors = {};

  if (!values.NAME) {
    errors.NAME = "Pflichtfeld";
  }
  if (!values.MCC_CODE) {
    errors.MCC_CODE = "Pflichtfeld";
  }
  if (!values.WEBSITE) {
    errors.WEBSITE = "Pflichtfeld";
  }
  if (!values.BANK_NAME) {
    errors.BANK_NAME = "Pflichtfeld";
  }
  if (!values.BANK_COUNTRY) {
    errors.BANK_COUNTRY = "Pflichtfeld";
  }
  if (!values.IBAN) {
    errors.IBAN = "Pflichtfeld";
  }
  if (!IBAN.isValid(values.IBAN)) {
    //errors.IBAN = "Invalid";
	  errors.IBAN = "Ungültige IBAN";
  }
  if (!values.ACCOUNTOWNER) {
    errors.ACCOUNTOWNER = "Pflichtfeld";
  }
  if (!values.SWIFT) {
    errors.SWIFT = "Pflichtfeld";
  }
  if (!values.COUNTRY) {
    errors.COUNTRY = "Pflichtfeld";
  }
  if (!values.COMPENSATION_ID) {
    errors.COMPENSATION_ID = "Pflichtfeld";
  }

  return errors;
};

export default validate;
