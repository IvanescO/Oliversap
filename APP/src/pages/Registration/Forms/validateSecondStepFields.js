const validate = values => {
  const errors = {};

  if (!values.email) {
    errors.email = "Pflichtfeld";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    //errors.email = "Invalid email address";
	  errors.email = "Ungültige email Addresse";
  }
  if (!values.FIRSTNAME) {
    errors.FIRSTNAME = "Pflichtfeld";
  }
  if (!values.LASTNAME) {
    errors.LASTNAME = "Pflichtfeld";
  }
  if (!values.NAME) {
    errors.NAME = "Pflichtfeld";
  }
  if (!values.WEBSITE) {
    errors.WEBSITE = "Pflichtfeld";
  }
  if (!values.INDUSTRY) {
    errors.INDUSTRY = "Pflichtfeld";
  }
  if (!values.CEO_ID) {
    errors.CEO_ID = "Pflichtfeld";
  }
  if (!values.REGISTRY_NUMBER) {
    errors.REGISTRY_NUMBER = "Pflichtfeld";
  }
  if (!values.REGISTRY_CITY) {
    errors.REGISTRY_CITY = "Pflichtfeld";
  }
  if (!values.TAX_NUMBER) {
    errors.TAX_NUMBER = "Pflichtfeld";
  }
  if (!values.VAT_NUMBER) {
    errors.VAT_NUMBER = "Pflichtfeld";
  }
  // if (!values.ACQUIRING_BANK) {
  //   errors.ACQUIRING_BANK = "Pflichtfeld";
  // }
  // if (!values.PSP_SOFTWARE) {
  //   errors.PSP_SOFTWARE = "Pflichtfeld";
  // }
  if (!values.SHOP_SYSTEM) {
    errors.SHOP_SYSTEM = "Pflichtfeld";
  }
  return errors;
};

export default validate;
