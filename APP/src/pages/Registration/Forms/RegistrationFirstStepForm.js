import React from 'react';
import { Fields, Field, reduxForm } from 'redux-form';
import { Row, Col, DropdownButton } from 'react-bootstrap';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import formColors from '../../../utils/formColors';
import ErrorMessageLabel from './ErrorMessageLabel';
import RenderField from './RenderField';
import RenderPassword from './RenderPassword';
import RenderCodeNumber from './RenderCodeNumber';
import RenderCountryZip from './RenderCountryZip';
import RenderNotesCheckBox from './RenderNotesCheckBox';
import validate from './validateFirstStepFields';
import { REGISTRATION_1STEP } from '../../../constants/index';

// form colors
const { ERROR_COLOR } = formColors;

//TODO: change to SELECTBOX
const renderSelectBox = ({
  input,
  label,
  renderItems,
  meta: { touched, error, warning }
}) => {
  return (
    <DropdownButton
      //componentClass={InputGroup.Button}
      id="input-dropdown-addon"
      title={input.value}
      className="drop-button border-right-radius-none"
      style={error ? { borderColor: ERROR_COLOR } : {}}>
      {renderItems()}
    </DropdownButton>
  );
};

const RegFirstStepStart = props => {
  const {
    handleSubmit,
    // pristine,
    // reset,
    // submitting,
    // registrationStart,
    // position,
    countriesInfo
  } = props;

  let submit = form => {
    const { firstStepComplete } = props;
    const router = props.history;
    console.log(form);
    firstStepComplete({ form, router });
    // registrationStart({ email, router });
  };

  return (
    <div>
      <Row className="input-block">
        <Col sm={12}>
          <Field
            name="EMAIL"
            label="EMAIL"
            id="EMAIL_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={6}>
          <Field
            name="PASSWORD"
            type="password"
            label="Passwort"
            id="PASSWORD_ID"
            component={RenderPassword}
          />
        </Col>
        <Col sm={6}>
          <Field
            name="PASSWORD_CONFIRM"
            type="password"
            label="Passwort bestätigen"
            id="PASSWORD_CONFIRM_ID"
            component={RenderPassword}
          />
        </Col>
      </Row>
      <Row>
        <Col sm={6}>
          <Field
            name="FIRSTNAME"
            label="Vorname"
            id="FIRSTNAME_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={6}>
          <Field
            name="LASTNAME"
            label="Nachname"
            id="LASTNAME_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12}>
          <Field
            name="NAME"
            label="Geschäftsname"
            id="NAME_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12}>
          <Field
            name="LEGAL_FORM"
            label="Rechtsform"
            id="LEGAL_FORM_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={12}>
          <Fields
            names={['phone_code', 'phone_number']}
            label={['Telefon Code', 'Telefon']}
            id={['phone_code_id', 'phone_number_id']}
            className={[
              'drop-button drop-inline-large border-left-radius-none'
            ]}
            data={countriesInfo}
            labelKey={'dial_code'}
            borderRight={true}
            component={RenderCodeNumber}
          />
        </Col>
        <Col sm={12}>
          <Field
            name="ADDRESS"
            label="Strasse (kein Postfach)"
            id="ADDRESS_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={4}>
          <Fields
            names={['COUNTRY', 'ZIP']}
            label={['Country code', 'PLZ']}
            id={['COUNTRY_ID', 'ZIP_ID']}
            className={['after-dropdown border-left-radius-none']}
            data={countriesInfo}
            labelKey={'code'}
            borderRight={false}
            component={RenderCountryZip}
          />
        </Col>
        <Col sm={4}>
          <Field
            name="STATE"
            label="Bundesland"
            id="STATE_ID"
            component={RenderField}
          />
        </Col>
        <Col sm={4}>
          <Field name="CITY" label="Ort" id="CITY_ID" component={RenderField} />
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <Field name="PRIVACYUSE" component={RenderNotesCheckBox} />
          <div className="checkbox-sub margin-bottom">
            <p>
              Datenschutz Nutzung Qui beatur aut que eum faccab ilit um alit,
              cum accus ad moluptusant, tem nimusaecto cus, si dedoluptatus as
              iunt autemquibus del is adi cum quos nobit etum quo te ad
              explacidit rectinis et verferum rectati blace
            </p>
          </div>
          <div className="align-center">
            <button onClick={handleSubmit(submit)} className="blue-bg">
              weiter
            </button>
          </div>
        </Col>
      </Row>
    </div>
  );
};

const withReduxForm = reduxForm({
  form: 'regFirstStepForm', // a unique identifier for this form
  validate
})(RegFirstStepStart);

const mapStateToProps = state => {
  const form = state.FormReducer.form;
  const countries = state.CountriesInfoReducer.countriesInfo;
  const userCountry =
    countries.find(e => {
      return e.code == form.COUNTRY;
    }) || {};
  return {
    initialValues: {
      COUNTRY: form.COUNTRY,
      PASSWORD: form.PASSWORD,
      PASSWORD_CONFIRM: form.PASSWORD_CONFIRM,
      FIRSTNAME: form.FIRSTNAME,
      LASTNAME: form.LASTNAME,
      NAME: form.NAME,
      LEGAL_FORM: form.LEGAL_FORM,
      CITY: form.CITY,
      ADDRESS: form.ADDRESS,
      ZIP: form.ZIP,
      phone_number: form.phone_number,
      phone_code: form.phone_code || userCountry.dial_code,
      EMAIL: form.EMAIL,
      PRIVACYUSE: form.PRIVACYUSE,
      STATE: form.STATE
    }
  };
};

const mapDispatchToProps = dispatch => {
  return {
    firstStepComplete: ({ form, router }) => {
      dispatch({ type: REGISTRATION_1STEP, form, router });
    },
    dispatch
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withReduxForm)
);
