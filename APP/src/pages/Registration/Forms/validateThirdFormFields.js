const validate = values => {
  const errors = {};

  if (!values.WEBSITE) {
    //errors.WEBSITE = "Required";
	  errors.WEBSITE = "Pflichtfeld";
  }
  if (!values.CONTRACTTYPE_ID) {
    //errors.CONTRACTTYPE_ID = "Required";
	  errors.CONTRACTTYPE_ID = "Pflichtfeld";
  }
  return errors;
};

export default validate;
