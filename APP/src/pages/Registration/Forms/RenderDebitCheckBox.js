import React from 'react';
import { Checkbox } from 'react-bootstrap';

const RenderDebitCheckBox = ({ input, text, chekBoxValue }) => (
  <Checkbox
    {...input}
    inline
    value={chekBoxValue}
    checked={input.value}
    className={input.value ? 'check active-box' : 'check'}>
    {text}
  </Checkbox>
);

export default RenderDebitCheckBox;
