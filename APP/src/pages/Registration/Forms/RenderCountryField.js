import React from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';
import { FormGroup } from 'react-bootstrap';
import ErrorMessageLabel from './ErrorMessageLabel';
import formColors from '../../../utils/formColors';

const { ERROR_COLOR } = formColors;

const RenderCountryField = ({
  input,
  label,
  data,
  id,
  labelKey,
  meta: { touched, error }
}) => {
  const isRequiredField = touched && error;

  return (
    <FormGroup
      controlId={id}
      validationState={isRequiredField ? 'error' : null}>
      <Typeahead
        {...input}
        labelKey={labelKey}
        multiple={false}
        defaultInputValue={input.value}
        inputProps={{
          style: {
            borderColor: touched && error ? formColors.ERROR_COLOR : ''
          }
        }}
        options={data}
        placeholder={label}
      />

      {isRequiredField ? (
        <ErrorMessageLabel
          errorText={error}
          errorColor={ERROR_COLOR}
          requiredField={isRequiredField}
        />
      ) : null}
    </FormGroup>
  );
};
export default RenderCountryField;
