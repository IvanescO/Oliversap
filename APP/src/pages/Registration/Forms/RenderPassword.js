import React from 'react';
import { FormGroup, FormControl } from "react-bootstrap";
import ErrorMessageLabel from "./ErrorMessageLabel";
import formColors from "../../../utils/formColors";

const { ERROR_COLOR } = formColors;

const RenderPassword = ({
  input,
  label,
  id,
  meta: { touched, error },
}) => {
  const isRequiredField = touched && error;

  return (
    <FormGroup controlId={id} validationState={ isRequiredField ? "error": null }>
      <FormControl
        {...input}
        style={isRequiredField ? { borderColor: ERROR_COLOR } : {}}
        type={"password"}
        placeholder={label}
      />

      <ErrorMessageLabel
        errorText={error}
        errorColor={ERROR_COLOR}
        requiredField={isRequiredField}
      />
    </FormGroup>
  );
};

export default RenderPassword;
