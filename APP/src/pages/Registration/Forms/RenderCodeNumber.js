import React from 'react';
import { Typeahead } from "react-bootstrap-typeahead";
import { Fields, Field } from "redux-form";
import { FormGroup, FormControl, ButtonGroup } from "react-bootstrap";
import ErrorMessageLabel from "./ErrorMessageLabel";
import formColors from "../../../utils/formColors";

const { ERROR_COLOR } = formColors;

const RenderCodeNumber = ({
  labelKey,
  borderRight,
  data,
  label,
  id,
  className,
  phone_code,
  phone_number,
}) => {
  const phoneCodeError = phone_code.meta.touched && phone_code.meta.error;
  const phoneNumberError = phone_number.meta.touched && phone_number.meta.error;
  const errorText = phone_number.meta.error || phone_number.meta.error;
  const isRequiredField = phoneCodeError || phoneNumberError;
  const isSelectId = id[0] || id[1];
  data = data.filter(e => e.dial_code);

  return (
    <FormGroup controlId={isSelectId} validationState={ isRequiredField ? "error": null }>
      <ButtonGroup className="drop-inline-small">
        <Typeahead
          {...phone_code.input}
          labelKey={labelKey}
          multiple={false}
          defaultInputValue={phone_code.input.value}
          inputProps={{
            style: {
              borderRight: borderRight ? "1 px" : "none",
              borderTopRightRadius: 0,
              borderBottomRightRadius: 0,
              borderColor: isRequiredField ? ERROR_COLOR : "",
            },
          }}
          options={data}
          placeholder={label[0]}
        />
      </ButtonGroup>
      <FormControl
        {...phone_number.input}
        className={className[0]}
        style={isRequiredField ? { borderColor: ERROR_COLOR } : {}}
        placeholder={label[1]}
      />

      { isRequiredField ?
        <ErrorMessageLabel
          errorText={errorText}
          errorColor={ERROR_COLOR}
          requiredField={isRequiredField}
        /> : null
      }
    </FormGroup>
  )
}

export default RenderCodeNumber;
