import React from 'react';
import { Checkbox } from 'react-bootstrap';

const RenderNotesCheckBox = ({ input, meta: { touched, error } }) => (
  <Checkbox
    {...input}
    inline
    checked={input.value}
    className={input.value ? 'notes check active-box' : 'notes check'}
  />
);

export default RenderNotesCheckBox;
