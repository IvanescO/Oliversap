import React from 'react';
import { FormGroup, FormControl } from 'react-bootstrap';
import ErrorMessageLabel from './ErrorMessageLabel';
import formColors from '../../../utils/formColors';

const { ERROR_COLOR } = formColors;

const RenderField = ({
  input,
  label,
  id,
  className,
  meta: { touched, error }
}) => {
  const isRequiredField = touched && error;

  return (
    <FormGroup
      controlId={id}
      validationState={isRequiredField ? 'error' : null}>
      <FormControl
        {...input}
        className={className}
        style={isRequiredField ? { borderColor: ERROR_COLOR } : {}}
        placeholder={label}
      />

      <ErrorMessageLabel
        errorText={error}
        errorColor={ERROR_COLOR}
        requiredField={isRequiredField}
      />
    </FormGroup>
  );
};

export default RenderField;
