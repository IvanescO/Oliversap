import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Grid,
  Col,
  Row,
  FormGroup,
  Radio,
  FormControl,
  DropdownButton,
  MenuItem,
  ButtonGroup,
  Checkbox,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import Wrapper from "../../components/Wrapper";
import { REGISTRATION_4STEP, GET_COUNTRY } from "../../constants/index";
import RegistrationFourthStepForm from "./Forms/RegistrationFourthStepForm";

export class RegistationFourthStep extends Component {
  state = {
    countriesInfo: [],
    step: "4step",
    form: {
      own_solution: false,
      NAME: "",
      COMPENSATION_ID: "",
      MCC_CODE: "",
      BANK_NAME: "", //TODO: ment it
      BANK_COUNTRY: "Land",
      IBAN: "",
      SWIFT: "",
      ACCOUNTOWNER: "",
    },
  };

  componentWillMount() {
    const { FormReducer, CountriesInfoReducer, getCountriesInfo } = this.props;
    getCountriesInfo();
    const { compensation, countriesInfo } = CountriesInfoReducer;
    this.setState({ compensation, countriesInfo });
  }
  componentWillReceiveProps(nextProps) {
    const { FormReducer, CountriesInfoReducer } = nextProps;
    const { сompensation, countriesInfo } = CountriesInfoReducer;
    this.setState({ сompensation, countriesInfo });
  }
  secondFourthComplete = () => {
    const { FormReducer, CountriesInfoReducer, getCountriesInfo } = this.props;
    const { secondFourthComplete } = this.props;
    const { form } = this.state;
    secondFourthComplete(form);
    // Try to get the ID form businessPartner and send to
    console.log("DEBUG Firmenname: " + form.NAME);
    //console.log("DEBUG Nachname: " + FormReducer.person.LASTNAME);
    //console.log("DEBUG ID: " + FormReducer.person.ID);
    console.log("DEBUG businessPartner.ID: " + FormReducer.businessPartner.ID);
    console.log("DEBUG FormReducer: " + FormReducer);
  };
  render() {
    const { form, compensation, countriesInfo } = this.state;
    return (
      <Wrapper headerColor={"purple"} hideButtons>
        <div>
          <div className="reg fifth-step">
            <Grid>
              <div className="wrapper">
                <div className="radio-block">
                  <FormGroup className="clearfix">
                    <Radio
                      className={this.state.step === "1step" && "active-circle"}
                      name="radioGroup"
                      value="1step"
                    >
                      Basics
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "2step" && "active-circle"}
                      value="2step"
                    >
                      Geschäftsinfos
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "3step" && "active-circle"}
                      value="3step"
                    >
                      Vertrag
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "4step" && "active-circle"}
                      value="4step"
                    >
                      Kontodatails
                    </Radio>
                  </FormGroup>
                </div>
                <div className="login-block">
                  <h2>Geschäftskonto eröffnen</h2>
                  <p>Ihre Kontodetails</p>
                  <RegistrationFourthStepForm
                    countriesInfo={countriesInfo}
                    compensation={compensation}
                  />
                </div>
              </div>
            </Grid>
          </div>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = ({ CountriesInfoReducer, FormReducer }) => ({
  CountriesInfoReducer,
  FormReducer,
});

const mapDispatchToProps = dispatch => {
  return {
    getCountriesInfo: () => {
      dispatch({ type: GET_COUNTRY });
    },
    secondFourthComplete: form => {
      dispatch({ type: REGISTRATION_4STEP, form });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistationFourthStep);
