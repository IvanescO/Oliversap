import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Grid,
  Form,
  Col,
  Row,
  FormGroup,
  Radio,
  FormControl,
  InputGroup,
  DropdownButton,
  MenuItem,
  Button,
  Checkbox,
  ControlLabel,
  Tab,
  Tabs,
  NavItem,
  Nav,
} from "react-bootstrap";
import { ButtonGroup } from "react-bootstrap";
import Wrapper from "../../components/Wrapper";
import { Link } from "react-router-dom";
import { REGISTRATION_3STEP } from "../../constants/index";
import RegistrationThirdStepForm from "./Forms/RegistrationThirdStepForm";

export class RegistationThirdStep extends Component {
  state = {
    step: "3step",
    form: {
      WEBSITE: "",
      CONTRACTTYPE_ID: "",
      own_solution: false,
    },
  };

  componentWillMount() {
    const { CountriesInfoReducer } = this.props;
    const { contracts } = CountriesInfoReducer;
    this.setState({ contracts });
  }
  componentWillReceiveProps(nextProps) {
    const { CountriesInfoReducer } = nextProps;
    const { contracts } = CountriesInfoReducer;

    this.setState({ contracts });
  }
  render() {
    let { contracts } = this.state;
    return (
      <Wrapper headerColor={"purple"} hideButtons>
        <div>
          <div className="reg third-step">
            <Grid>
              <div className="wrapper">
                <div className="radio-block">
                  <FormGroup className="clearfix">
                    <Radio
                      className={this.state.step === "1step" && "active-circle"}
                      name="radioGroup"
                      value="1step"
                      //   checked={this.state.step === "1step"}
                      //   onChange={this.onChangeStep}
                    >
                      Basics
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "2step" && "active-circle"}
                      value="2step"
                      //   onChange={this.onChangeStep}
                      //   checked={this.state.step === "2step"}
                    >
                      Geschäftsinfos
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "3step" && "active-circle"}
                      value="3step"
                      //   onChange={this.onChangeStep}
                      //   checked={this.state.step === "3step"}
                    >
                      Vertrag
                    </Radio>
                    <Radio
                      name="radioGroup"
                      className={this.state.step === "4step" && "active-circle"}
                      value="4step"
                      //   onChange={this.onChangeStep}
                      //   checked={this.state.step === "4step"}
                    >
                      Kontodatails
                    </Radio>
                  </FormGroup>
                </div>

                <div className="login-block">
                  <h2>Geschäftskonto eröffnen</h2>
                  <p>Vertrag beantragen</p>
                  <RegistrationThirdStepForm contracts={contracts} />
                </div>
              </div>
            </Grid>
          </div>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = ({ CountriesInfoReducer, FormReducer }) => ({
  CountriesInfoReducer,
  FormReducer,
});

const mapDispatchToProps = dispatch => {
  return {
    secondThirdComplete: form => {
      dispatch({ type: REGISTRATION_3STEP, form });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistationThirdStep);
