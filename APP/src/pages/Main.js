import React, { Component } from "react";
import { Col, Row, Grid } from "react-bootstrap";
import { connect } from "react-redux";
import Wrapper from "../components/Wrapper";
import Link from "react-router-dom/Link";
import {
  GET_COUNTRY,
  GET_CONTRACTS,
  GET_COMPENSATIONS,
} from "../constants/index";

export class Main extends Component {
  componentWillMount() {
    const { getCountriesInfo, getContracts, getCompensation } = this.props;
    getCountriesInfo();
    getContracts();
    getCompensation();
  }
  render() {
    return (
      <Wrapper headerColor={""} hideButtons>
        <div className="main">
          <div className="banner">
            <Grid>
              <Row className="justify-content-center heading">
                <Col md={12} sm={12}>
                  <h1>
                    Vollautomatisiertes Mehrwertsteuer Clearing für alle
                    E-Commerce Unternehmen
                  </h1>
                  <Link to={"/registration"}>
                    <button>Anmelden</button>
                  </Link>
                  <p>Jetzt Geschäftskonto Eröffnen</p>
                </Col>
              </Row>
            </Grid>
          </div>
          <div className="features">
            <Grid>
              <h2>Clearvat - eine lösung für alle eu länder</h2>
              <Row>
                <Col lg={4} md={6} sm={6}>
                  <span className="svg icon-display" />
                  <h3>Display</h3>
                  <p>
                    ClearVAT nihilig enduciu sandit dita dolorum sed molorem
                    volor re moluptam cupta quaecta conecesto magnisimigenda
                    dolorrovit eicillu ptatus.
                  </p>
                  <button className="blue-border">mehr erfahren</button>
                </Col>
                <Col lg={4} md={6} sm={6}>
                  <span className="svg icon-threshold" />
                  <h3>Threshold control</h3>
                  <p>
                    ClearVAT nihilig enduciu sandit dita dolorum sed molorem
                    volor re moluptam cupta quaecta conecesto magnisimigenda
                    dolorrovit eicillu ptatus.
                  </p>
                  <button className="blue-border">mehr erfahren</button>
                </Col>
                <Col
                  lg={4}
                  lgOffset={0}
                  md={6}
                  mdOffset={3}
                  sm={6}
                  smOffset={3}
                >
                  <span className="svg icon-collect" />
                  <h3>Collect &amp; Clear</h3>
                  <p>
                    ClearVAT nihilig enduciu sandit dita dolorum sed molorem
                    volor re moluptam cupta quaecta conecesto magnisimigenda
                    dolorrovit eicillu ptatus.
                  </p>
                  <button className="blue-border">mehr erfahren</button>
                </Col>
              </Row>
            </Grid>
          </div>
          <div className="info-block">
            <Grid>
              <h2>Wir machen das mit der mwst!</h2>
              <p>So einfach funktioniert ClearVAT</p>
              <Row>
                <Col lg={4} md={6} sm={6}>
                  <span className="number">1</span>
                  <h3>Software holen</h3>
                  <span className="svg icon-expandieren" />
                  <p>
                    Downloaden Sie konstenlos das ClearVAT Plug-in für ihren
                    Shop.
                  </p>
                </Col>
                <Col lg={4} md={6} sm={6}>
                  <span className="number">2</span>
                  <h3>Vertrag Einreichen</h3>
                  <span className="svg icon-vertrag" />
                  <p>
                    Füllen Sie alle Antragsfelder aus und senden Sie den
                    unterschriebenen Vertrag ein.
                  </p>
                </Col>
                <Col
                  lg={4}
                  lgOffset={0}
                  md={6}
                  mdOffset={3}
                  sm={6}
                  smOffset={3}
                >
                  <span className="number">3</span>
                  <h3>Expandieren</h3>
                  <span className="svg icon-software" />
                  <p>
                    Verkaufen Sie problemlos in ganz Europa grenzübergreifend.
                  </p>
                </Col>
              </Row>
              <Link to="/registration">
                <button className="blue-border">los gehts</button>
              </Link>
            </Grid>
          </div>
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => {
  return {
    getCountriesInfo: () => {
      dispatch({ type: GET_COUNTRY });
    },
    getContracts: () => {
      dispatch({ type: GET_CONTRACTS });
    },
    getCompensation: () => {
      dispatch({ type: GET_COMPENSATIONS });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
