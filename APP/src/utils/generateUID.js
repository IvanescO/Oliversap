export default function generateUID() {
  return (Date.now() / Math.random() * Math.random()).toFixed(0);
}
