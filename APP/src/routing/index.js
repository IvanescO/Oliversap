import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Main from "../pages/Main";
import {
  RegistrationStart,
  RegistrationFirstStep,
  RegistrationSecondtStep,
  RegistationThirdStep,
  RegistationFourthStep,
  RegistrationFinish,
} from "../pages/Registration";
import ScrollToTop from "../components/ScrollToTop";
import PreValidationPage from "../pages/Registration/PrevalidationPage";

const Routing = () => {
  return (
    <div>
      <Router>
        <ScrollToTop>
          <Route exact path={"/"} component={Main} />
          <Route path={"/registration"} component={RegistrationStart} />
          <Route
            path={"/registration-1step"}
            component={RegistrationFirstStep}
          />
          <Route
            path={"/registration-2step"}
            component={RegistrationSecondtStep}
          />
          <Route
            path={"/registration-3step"}
            component={RegistationThirdStep}
          />
          <Route
            path={"/registration-4step"}
            component={RegistationFourthStep}
          />
          <Route path={"/registration-finish"} component={RegistrationFinish} />
          <Route path={"/prevalidation-page"} component={PreValidationPage} />
        </ScrollToTop>
      </Router>
    </div>
  );
};

export default Routing;
