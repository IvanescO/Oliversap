import { takeEvery, call, put } from "redux-saga/effects";
import {
  GET_COUNTRY,
  GET_COUNTRY_SUCCESSFUL,
  FORM_STORING,
} from "../constants";
import { CountryService } from "../services";

function* getCountyInfo(action) {
  let countriesInfo,
    currentPost = { city: "", countryCode: "", country: "", zip: "" };
  try {
    const response = yield call(CountryService.getCountriesInfo);
    countriesInfo = response.data;
  } catch (error) {
    //TODO: ERROR HANDLING
    console.log(error);
  }
  try {
    const getCurrentPosResponse = yield call(CountryService.getCurrentData);
    currentPost = getCurrentPosResponse.data;
    currentPost = {
      city: getCurrentPosResponse.data.city,
      countryCode: getCurrentPosResponse.data.country,
      //country: getCurrentPosResponse.data.country_name,
      country: getCurrentPosResponse.data.region,
      dial_code: getCurrentPosResponse.data.country_calling_code,
      zip: getCurrentPosResponse.data.postal,
    };
    console.log(currentPost);
  } catch (error) {
    //TODO: ERROR HANDLING
    console.log(error);
  }
  yield put({
    type: FORM_STORING,
    form: {
      CITY: currentPost.city,
      COUNTRY: currentPost.countryCode,
      STATE: currentPost.country,
      ZIP: currentPost.zip,
    },
  });
  yield put({ type: GET_COUNTRY_SUCCESSFUL, countriesInfo });
}
const CountrySagas = [takeEvery(GET_COUNTRY, getCountyInfo)];
export default CountrySagas;
