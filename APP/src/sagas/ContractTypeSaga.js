import { takeEvery, call, put, all } from "redux-saga/effects";
import { GET_CONTRACTS, GET_CONTRACTS_SUCCESSFUL } from "../constants";
import { ContractTypeService } from "../services";

function* getContracts(action) {
  try {
    const response = yield call(ContractTypeService.getTypes);
    let contracts = response.data.d.AppKeyRef.results;

    let promises = contracts.map(e => {
      return call(ContractTypeService.getTypeText, e.ID);
    });
    let promisesForPrice = contracts.map(e => {
      return call(ContractTypeService.getTypePrice, e.ID);
    });
    const responsesPrices = yield all(promisesForPrice);
    const DEprices = responsesPrices.map(e => {
      return e.data.d.TranslationRef.results;
    }).map(e =>  e.filter(e =>e.LANGUAGE == "DE"))
    console.log(DEprices)
    const responsesText = yield all(promises);
    const DEtext = responsesText.map(e => {
      return e.data.d.TranslationRef.results;
    }).map(e =>  e.filter(e =>e.LANGUAGE == "DE"))
    console.log(DEtext)
    contracts = responsesText.map((e,i) => {
      return {
        ID: e.data.d.ID,
        TEXT: e.data.d.TranslationRef.results[0].NAME,
        PRICE: DEprices[i][0].NAME
      };
    });
    console.log(contracts)
    yield put({ type: GET_CONTRACTS_SUCCESSFUL, contracts });
    // yield put({ type: GET_COUNTRY_SUCCESSFUL, countriesInfo });
  } catch (error) {
    //TODO: ERROR HANDLING
    console.log(error);
  }
}
const ContractTypes = [takeEvery(GET_CONTRACTS, getContracts)];
export default ContractTypes;
