import { takeEvery, call, put } from "redux-saga/effects";
import { CREATE_PERSON, CREATE_PERSON_SUCCESSFUL } from "../constants";
import {
  PersonService,
  ContactDataService,
  AuthLoginService,
} from "../services";
import { generateUID } from "../utils";

function* createPerson(action) {
  try {
    const person = { ID: generateUID() };
    const { email } = action;
    const contactData = {
      ID: generateUID(),
      NAME: "email",
      VALUE: email,
      PERSON_ID: person.ID,
    };
    const authLogin = {
      ID: generateUID(),
      USERNAME: email,
      PERSON_ID: person.ID,
    };

    const responsePerson = yield call(PersonService.CreatePerson, person);
    const responceContactData = yield call(
      ContactDataService.CreateContactData,
      contactData
    );
    const responceAuthLogin = yield call(
      AuthLoginService.CreateAuth,
      authLogin
    );
    yield put({
      type: CREATE_PERSON_SUCCESSFUL,
      contactData,
      authLogin,
      person,
    });
  } catch (error) {
    //TODO: ERROR HANDLING
    console.log(error);
  }
}
const PersonSagas = [takeEvery(CREATE_PERSON, createPerson)];
export default PersonSagas;
