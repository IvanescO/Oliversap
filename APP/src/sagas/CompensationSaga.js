import { takeEvery, call, put, all } from "redux-saga/effects";
import { GET_COMPENSATIONS, GET_COMPENSATIONS_SUCCESSFUL } from "../constants";
import { CompensationService } from "../services";

function* getCompensations(action) {
  try {
    const response = yield call(CompensationService.getCompensations);
    let compensation = response.data.d.AppKeyRef.results;

    let promises = compensation.map(e => {
      return call(CompensationService.getCompensText, e.ID);
    });
    const responsesText = yield all(promises);
    compensation = responsesText.map(e => {
      return {
        ID: e.data.d.ID,
        TEXT: e.data.d.TranslationRef.results[1].NAME,
      };
    });
    yield put({ type: GET_COMPENSATIONS_SUCCESSFUL, compensation });
    // yield put({ type: GET_COUNTRY_SUCCESSFUL, countriesInfo });
  } catch (error) {
    //TODO: ERROR HANDLING
    console.log(error)
  }
}
const CompensationSaga = [takeEvery(GET_COMPENSATIONS, getCompensations)];
export default CompensationSaga;
