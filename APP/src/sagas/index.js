import { all } from "redux-saga/effects";
import CountrySaga from "./CountrySaga";
import RegistrationSaga from "./RegistrationSaga";
import ContractTypeSaga from "./ContractTypeSaga";
import CompensationSaga from "./CompensationSaga";
export default function* rootSaga() {
  yield all([
    ...CountrySaga,
    ...RegistrationSaga,
    ...ContractTypeSaga,
    ...CompensationSaga,
  ]);
}
