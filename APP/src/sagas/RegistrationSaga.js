import { takeEvery, call, put, select, all } from "redux-saga/effects";
import {
  REGISTRATION_START,
  REGISTRATION_START_SUCCESSFUL,
  REGISTRATION_1STEP,
  REGISTRATION_1STEP_SUCCESSFUL,
  FORM_STORING,
  REGISTRATION_2STEP,
  REGISTRATION_3STEP,
  REGISTRATION_2STEP_SUCCESSFUL,
  REGISTRATION_3STEP_SUCCESSFUL,
  REGISTRATION_4STEP,
  REGISTRATION_4STEP_SUCCESSFUL,
} from "../constants";
import {
  PersonService,
  ContactDataService,
  AuthLoginService,
  BusinessPartnerService,
  AddressService,
  ServiceContractService,
  BankDetailService,
} from "../services";
import { generateUID } from "../utils";
import TenantService from "../services/TenantService";
import WorkFlowService from "../services/WorkFlowService";

function* RegistrationStart(action) {
  try {
    const person = { ID: generateUID() };
    const { email, router } = action;
    const form = {
      EMAIL: email,
      WEBSITE: `http://www.${email.split("@")[1]}`,
    };
    yield put({ type: FORM_STORING, form });

    router.push("/registration-1step");

    const contactData = {
      ID: generateUID(),
      NAME: "email",
      VALUE: email,
      PERSON_ID: person.ID,
    };
    const authLogin = {
      ID: generateUID(),
      USERNAME: email,
      PERSON_ID: person.ID,
    };

    const responsePerson = yield call(PersonService.CreatePerson, person);

    const [responceContactData, responceAuthLogin] = yield all([
      call(ContactDataService.CreateContactData, contactData),
      call(AuthLoginService.CreateAuth, authLogin),
    ]);

    yield put({
      type: REGISTRATION_START_SUCCESSFUL,
      contactData,
      authLogin,
      person,
    });
  } catch (e) {
    //TODO: ERROR HANDLING
    console.log(e);
  }
}
function* RegistrationFirstStep(action) {
  try {
    const { form, router } = action;
    yield put({ type: FORM_STORING, form });
    let {
      FormReducer: { contactData, authLogin, person },
    } = yield select();

    router.push("/registration-2step");
    authLogin = {
      ...authLogin,
      USERNAME: form.USERNAME,
      PASSWORD: form.PASSWORD,
    };
    person = {
      ...person,
      FIRSTNAME: form.FIRSTNAME,
      LASTNAME: form.LASTNAME,
      PRIVACYUSE: form.PRIVACYUSE ? 1 : 0,
    };

    // let tenant = {
    //   ID: generateUID(),
    // };
    let address = {
      ID: generateUID(),
      ADDRESS: form.ADDRESS,
      CITY: form.CITY,
      STATE: form.STATE,
      ZIP: form.ZIP,
      COUNTRY: form.COUNTRY,
    };
    let businessPartner = {
      ID: generateUID(),
      NAME: form.NAME,
      LEGAL_FORM: form.LEGAL_FORM,
      PHONE: form.PHONE,
      MAINADDRESS_ID: address.ID,
      MAINCONTACT_ID: contactData.ID,
      TENANT_ID: "4",
    };

    const [authLoginResponse, personResponse] = yield all([
      call(AuthLoginService.UpdateAuth, authLogin),
      call(PersonService.UpdatePerson, person),
      // call(TenantService.CreateTenant, tenant),
    ]);
    const businessPartnerResponse = yield call(
      BusinessPartnerService.CreateBusinessPartner,
      businessPartner
    );
    const addressResponse = yield call(AddressService.CreateAddress, address);
    address = { ...address, BUSINESSPARTNER_ID: businessPartner.ID };
    yield call(AddressService.UpdateAddress, address);
    yield put({
      type: REGISTRATION_1STEP_SUCCESSFUL,
      authLogin,
      businessPartner,
      person,
      address,
    });
    console.log(
      authLoginResponse,
      personResponse,
      businessPartnerResponse,
      addressResponse
    );
  } catch (e) {
    //TODO: ERROR HANDLING
    console.log(e);
  }
}
function* RegistrationSecondStep(action) {
  try {
    const { form, router } = action;
    yield put({ type: FORM_STORING, form });
    let {
      FormReducer: { contactData, authLogin, person, businessPartner },
    } = yield select();
    router.push("/registration-3step");
    let serviceContract = {
      ID: generateUID(),
      SHOP_SYSTEM: form.SHOP_SYSTEM,
      ACQUIRING_BANK: form.ACQUIRING_BANK,
      PSP_SOFTWARE: form.PSP_SOFTWARE,
    };
    const result = form.CEO_ID.split(" ");
    let lastname = result.pop();
    let firstname = result.join(" ");
    let CEO = {
      ID: generateUID(),
      FIRSTNAME: firstname,
      LASTNAME: lastname || "",
    };
    businessPartner = {
      ...businessPartner,
      INDUSTRY: form.INDUSTRY,
      WEBSITE: form.WEBSITE,
      CEO_ID: CEO.ID,
      REGISTRY_NUMBER: form.REGISTRY_NUMBER,
      REGISTRY_CITY: form.REGISTRY_CITY,
      TAX_NUMBER: form.TAX_NUMBER,
      VAT_NUMBER: form.VAT_NUMBER,
      SERVICECONTRACT_ID: serviceContract.ID,
    };
    person = {
      ...person,
      LANGUAGE: form.LANGUAGE,
    };

    const [personResponse, serviceContractResponce, ceoResponse] = yield all([
      call(PersonService.UpdatePerson, person),
      call(ServiceContractService.CreateServiceContract, serviceContract),
      call(PersonService.CreatePerson, CEO),
    ]);
    const businessPartnerResponse = yield call(
      BusinessPartnerService.UpdateBusinessPartner,
      businessPartner
    );
    yield put({
      type: REGISTRATION_2STEP_SUCCESSFUL,
      serviceContract,
      businessPartner,
      person,
    });
  } catch (e) {
    //TODO: ERROR HANDLING
    console.log(e);
  }
}
function* RegistrationThirdStep(action) {
  try {
    const { form, router } = action;
    yield put({ type: FORM_STORING, form });
    let {
      FormReducer: { serviceContract },
    } = yield select();
    serviceContract = {
      ...serviceContract,
      CONTRACTTYPE_ID: form.CONTRACTTYPE_ID,
    };
    router.push("/registration-4step");
    const serviceContractResponse = yield call(
      ServiceContractService.UpdateServiceContract,
      serviceContract
    );
    yield put({ type: REGISTRATION_3STEP_SUCCESSFUL, serviceContract });
  } catch (e) {
    //TODO: ERROR HANDLING
    console.log(e);
  }
}
function* RegistrationFourthStep(action) {
  try {
    console.log("Start RegistrationFourthStep");
    const { form, router } = action;
    yield put({ type: FORM_STORING, form });

    let {
      FormReducer: { serviceContract, businessPartner },
    } = yield select();
    router.push("/prevalidation-page");
    let bankDetail = {
      ID: generateUID(),
      NAME: form.BANK_NAME,
      COUNTRY: form.BANK_COUNTRY,
      IBAN: form.IBAN,
      SWIFT: form.SWIFT,
      DEBIT_AUTHORIZATION: form.DEBIT_AUTHORIZATION ? 1 : 0,
      ACCOUNTOWNER: form.ACCOUNTOWNER,
    };
    serviceContract = {
      ...serviceContract,
      COMPENSATION_ID: form.COMPENSATION_ID,
    };
    businessPartner = {
      ...businessPartner,
      MCC_CODE: form.MCC_CODE,
    };
    let tokenResponse = yield call(WorkFlowService.getCSRFToken);
    let token = tokenResponse.headers["x-csrf-token"];

    const workFlowResponse = yield call(
      WorkFlowService.startInstance,
      {
        definitionId: "onboarding",
        context: {
          BusinessPartner: businessPartner,
        },
      },
      token
    );
    console.log(workFlowResponse);
    const [personResponse, serviceContractResponce] = yield all([
      call(BusinessPartnerService.UpdateBusinessPartner, businessPartner),
      call(ServiceContractService.UpdateServiceContract, serviceContract),
      call(BankDetailService.CreateBankDetail, bankDetail),
    ]);

    yield put({
      type: REGISTRATION_4STEP_SUCCESSFUL,
      bankDetail,
      serviceContract,
      businessPartner,
    });
  } catch (e) {
    //TODO: ERROR HANDLING
    console.log(e);
  }
}

const RegistrationSaga = [
  takeEvery(REGISTRATION_START, RegistrationStart),
  takeEvery(REGISTRATION_1STEP, RegistrationFirstStep),
  takeEvery(REGISTRATION_2STEP, RegistrationSecondStep),
  takeEvery(REGISTRATION_3STEP, RegistrationThirdStep),
  takeEvery(REGISTRATION_4STEP, RegistrationFourthStep),
];
export default RegistrationSaga;
