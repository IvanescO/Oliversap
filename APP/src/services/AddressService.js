import http from "./http";
import config from "./http/config";

class AddressService {
  static CreateAddress(Address) {
    return http.post("/Address", Address);
  }
  static UpdateAddress(Address) {
    return http.put(`/Address(${Address.ID}L)`, Address);
  }
}
export default AddressService;
