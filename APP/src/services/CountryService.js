import axios from "axios";
import config from "./http/config";

class CountryService {
  static getCountriesInfo() {
    return axios.get(config.countryUrl);
  }
  static getCurrentData() {
    return axios.get("https://ipapi.co/json");
  }
}
export default CountryService;
