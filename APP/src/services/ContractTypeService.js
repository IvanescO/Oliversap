import http from "./http";
import config from "./http/config";

class ContractTypeService {
  static getTypes(contactData) {
    return http.get(
      "/AppKeyGroup('clearvat_contract_types')/?$expand=AppKeyRef"
    );
  }
  static getTypeText(type) {
    return http.get(`/AppKey('${type}')/?$expand=TranslationRef`);
  }
  static getTypePrice(type) {
    return http.get(`/AppKey('${type}_pricing')/?$expand=TranslationRef`);
  }
}

export default ContractTypeService;
