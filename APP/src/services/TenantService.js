import http from "./http";
import config from "./http/config";

class TenantService {
  static CreateTenant(Tenant) {
    return http.post("/Tenant", Tenant);
  }
  static UpdateTenant(Tenant) {
    return http.put(`/Tenant(${Tenant.ID}L)`, Tenant);
  }
}
export default TenantService;
