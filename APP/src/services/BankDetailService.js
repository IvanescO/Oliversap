import http from "./http";
import config from "./http/config";

class BankDetailService {
  static CreateBankDetail(BankDetail) {
    return http.post("/BankDetail", BankDetail);
  }
  static UpdateBankDetail(BankDetail) {
    return http.put(`/BankDetail(${BankDetail.ID}L)`, BankDetail);
  }
}
export default BankDetailService;
