import http from "./http";
import config from "./http/config";

class PersonService {
  static CreatePerson(person) {
    return http.post("/Person", person);
  }
  static UpdatePerson(person) {
    return http.put(`/Person(${person.ID}L)`, person);
  }
}
export default PersonService;
