import http from "./http";
import config from "./http/config";

class ContactDataService {
  static CreateContactData(contactData) {
    return http.post("/ContactData", contactData);
  }
  static UpdateContactData(contactData) {
    return http.put(`/ContactData(${contactData}L)`, contactData);
  }
}
export default ContactDataService;
