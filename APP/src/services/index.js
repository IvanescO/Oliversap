import CountryService from "./CountryService";
import PersonService from "./PersonService";
import ContactDataService from "./ContactDataService";
import AuthLoginService from "./AuthLoginService";
import BusinessPartnerService from "./BusinessPartnerService";
import AddressService from "./AddressService";
import ServiceContractService from "./ServiceContractService";
import ContractTypeService from "./ContractTypeService";
import CompensationService from "./CompensationService";
import BankDetailService from "./BankDetailService";
import TenantService from "./TenantService";
export {
  CountryService,
  PersonService,
  ContactDataService,
  CompensationService,
  AuthLoginService,
  BusinessPartnerService,
  AddressService,
  BankDetailService,
  ContractTypeService,
  ServiceContractService,
};
