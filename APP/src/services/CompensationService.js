import http from "./http";
import config from "./http/config";

class CompensationService {
  static getCompensations(contactData) {
    return http.get(
      "AppKeyGroup('clearvat_compensation_types')/?$expand=AppKeyRef"
    );
  }
  static getCompensText(type) {
    return http.get(`AppKey('${type}')/?$expand=TranslationRef`);
  }
}

export default CompensationService;
