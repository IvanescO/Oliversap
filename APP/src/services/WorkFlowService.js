import axios from "axios";
import config from "./http/config";

const http = axios.create({
  baseURL: config.workflowEngineApi,
});
http.interceptors.response.use(
  function(response) {
    // Do something with response data
    console.log(response);
    return response;
  },
  function(error) {
    console.log(error);
    // Do something with response error
    return Promise.reject(error);
  }
);
http.interceptors.request.use(
  async req => {
    const originalRequest = req;
    // originalRequest.auth = {
    //   username: "P2000355777",
    //   password: "Lazarev12345@",
    // };
    // originalRequest.headers.Authorization = `Bearer ${TOKEN}`;
    console.log(originalRequest);
    return originalRequest;
  },
  err => Promise.reject(err)
);
class WorkFlowService {
  static getCSRFToken(Tenant) {
    return http.get("/xsrf-token", {
      headers: {
        "X-Requested-With": "XMLHttpRequest",

        "Content-Type": "application/atom+xml",

        DataServiceVersion: "2.0",

        "X-CSRF-Token": "Fetch",
      },
    });
  }
  static startInstance(data, token) {
    return http.post("/workflow-instances", data, {
      headers: { "X-CSRF-Token": token },
    });
  }
}
export default WorkFlowService;
