import http from "./http";
import config from "./http/config";

class AuthLoginService {
  static CreateAuth(AuthLogin) {
    return http.post("/AuthLogin", AuthLogin);
  }
  static UpdateAuth(AuthLogin) {
    return http.put(`/AuthLogin(${AuthLogin.ID}L)`, AuthLogin);
  }
}
export default AuthLoginService;
