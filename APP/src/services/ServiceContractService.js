import http from "./http";
import config from "./http/config";

class ServiceContractService {
  static CreateServiceContract(ServiceContract) {
    return http.post("/ServiceContract", ServiceContract);
  }
  static UpdateServiceContract(ServiceContract) {
    return http.put(
      `/ServiceContract(${ServiceContract.ID}L)`,
      ServiceContract
    );
  }
}
export default ServiceContractService;
