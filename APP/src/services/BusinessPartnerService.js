import http from "./http";
import config from "./http/config";

class BusinessPartnerService {
  static CreateBusinessPartner(BusinessPartner) {
    return http.post("/BusinessPartner", BusinessPartner);
  }
  static UpdateBusinessPartner(BusinessPartner) {
    return http.put(
      `/BusinessPartner(${BusinessPartner.ID}L)`,
      BusinessPartner
    );
  }
}
export default BusinessPartnerService;
