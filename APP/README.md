# Olive SAP PROJECT

Demonstration application

## Prerequisites

- Install [NodeJs](https://nodejs.org)
- Install these NPM packages globally

    ```bash
    npm install -g create-react-app
    ```

### Installing Packages

 - `npm install`

### Running in dev mode
 - Run the project with `npm start` 
 - Open browser http://localhost:3000/

### Build of the project
 - Run this command `npm run build`
 - Build folder will be generated